package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.models.UserModel;
import com.gaming.project.gamingproject.repositories.AuthoritiesRepository;
import com.gaming.project.gamingproject.repositories.LikeCommentRepository;
import com.gaming.project.gamingproject.repositories.LoginRepository;
import com.gaming.project.gamingproject.repositories.RequestRepository;
import com.gaming.project.gamingproject.repositories.UserRepository;
import com.gaming.project.gamingproject.services.contracts.AuthoritiesService;
import com.gaming.project.gamingproject.services.contracts.ConfirmationTokenService;
import com.gaming.project.gamingproject.services.contracts.FriendService;
import com.gaming.project.gamingproject.services.contracts.LikeCommentService;
import com.gaming.project.gamingproject.services.contracts.LoginService;
import com.gaming.project.gamingproject.services.contracts.RequestService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

  @InjectMocks
  private UserServiceImpl userService;

  @Mock
  private LikeCommentService likeCommentService;

  @Mock
  private LikeCommentRepository likeCommentRepository;

  @Mock
  private AuthoritiesService authoritiesService;

  @Mock
  private AuthoritiesRepository authoritiesRepository;

  @Mock
  private RequestService requestService;

  @Mock
  private FriendService friendService;

  @Mock
  private Principal principal;

  @Mock
  private UserRepository userRepository;

  @Mock
  private LoginRepository loginRepository;

  @Mock
  private RequestRepository requestRepository;

  @Mock
  private ModelMapper modelMapper;

  @Mock
  private LoginService loginService;

  @Mock
  private ConfirmationTokenService tokenService;

  private final static long USER_ID = 1;
  
  private User testUser = new User();
  private User loggedUser = new User();

  @Before
  public void setUp() {
    testUser.setId(USER_ID);
    testUser.setLogin(new Login("ivan", "USER_ID2332USER_ID", true));
  }

  @Test
  public void addUser_Should_addUserSuccessfully() {
    userService.addUser(new User());

    verify(userRepository).save(Mockito.any());
  }

  @Test
  public void findById_Should_ReturnRightUser_WhenExist() {
    when(userRepository.findById(USER_ID)).thenReturn(Optional.of(new User()));
    User userResult = userService.findById(USER_ID);
    userResult.setId(USER_ID);

    assertEquals(Long.valueOf(1), userResult.getId());
  }

  @Test
  public void findByIdModel_Should_ReturnRightUserModel_WhenExist() {
    // GIVEN
    UserModel userModel = new UserModel();
    userModel.setId(USER_ID);
    User user = new User();
    user.setId(USER_ID);

    when(userRepository.findById(USER_ID)).thenReturn(Optional.of(user));
    when(userService.findByIdModel(USER_ID)).thenReturn(userModel);

    //WHEN
    UserModel userResult = userService.findByIdModel(USER_ID);

    //THEN
    assertEquals(Optional.of(USER_ID), Optional.ofNullable(userResult.getId()));
  }

  @Test
  public void countUsers_Should_ReturnRight_amount() {

    when(userRepository.count()).thenReturn(Long.valueOf(2));

    assertEquals(Long.valueOf(2), userService.countUsers());
  }

  @Test
  public void createUser_Should_ReturnNewUserAdded() {

    Login login = new Login();
    login.setUsername("TestIvan");
    login.setEnabled(true);

    when(loginService.findByUsername(Mockito.any())).thenReturn(login);

    RegisterUserModel registerUserModel = new RegisterUserModel();
    registerUserModel.setFirstName("Ivan");
    registerUserModel.setLastName("Ivanov");
    registerUserModel.setEmail("Ivan@abv.bg");
    registerUserModel.setAge(15);
    registerUserModel.setGender(GenderType.MALE);
    registerUserModel.setCountry("Bulgaria");
    registerUserModel.setPicture("avatar3.png");
    registerUserModel.setLogin(login);

    userService.createUser(registerUserModel);

    verify(userRepository).save(Mockito.any());
  }

  @Test
  public void findAllByGenderType_Should_return_listOfUsers() {
    List<User> users = new ArrayList<>();
    users.add(new User());
    users.add(new User());

    when(userRepository.findByGenderEquals(GenderType.MALE)).thenReturn(users);

    assertEquals(2, userService.findAllByGenderType(GenderType.MALE).size());
  }

  @Test(expected = EntityNotFoundException.class)
  public void deleteUserById_Should_throw_exception_whenUserNotFound() {

    userService.deleteUserById(USER_ID);

    verify(userRepository).delete(testUser);
  }

  @Test
  public void deleteUserById_Should_remove_user() {
    when(userRepository.existsById(USER_ID)).thenReturn(true);
    when(userRepository.findById(USER_ID)).thenReturn(Optional.ofNullable(testUser));

    userService.deleteUserById(USER_ID);

    verify(userRepository).delete(testUser);
  }

}
