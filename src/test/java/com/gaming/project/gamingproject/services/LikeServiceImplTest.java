package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Like;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityAlreadyExist;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.LikeCommentModel;
import com.gaming.project.gamingproject.models.LikeModel;
import com.gaming.project.gamingproject.repositories.LikeRepository;
import com.gaming.project.gamingproject.repositories.PostRepository;
import com.gaming.project.gamingproject.repositories.UserRepository;
import com.gaming.project.gamingproject.services.contracts.PostService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LikeServiceImplTest {
  
  @InjectMocks
  private LikeServiceImpl likeService;

  @Mock
  private LikeRepository likeRepository;

  @Mock
  private PostService postService;

  @Mock
  private PostRepository postRepository;

  @Mock
  private UserServiceImpl userService;

  @Mock
  private UserRepository userRepository;

  @Mock
  private Principal principal;

  private final static long USER_ID = 1;

  private Like testLike = new Like();
  private User testUser = new User();
  private Post testPost = new Post();
  private LikeCommentModel likeCommentModel = new LikeCommentModel();
  private LikeModel likeModel = new LikeModel();

  @Before
  public void setUp() {
    likeCommentModel.setCommentId(USER_ID);
    likeCommentModel.setUserId(USER_ID);

    testLike.setId(USER_ID);

    testUser.setId(USER_ID);
    testUser.setLogin(new Login("ivan", "USER_ID2332USER_ID", true));
    testUser.setFirstName("Ivan");

    testPost.setId(USER_ID);

    likeModel.setPostId(USER_ID);
  }

  @Test
  public void existsByPostIdAndUserId_Should_returnInformationAboutExisting() {
    when(likeService.existsByPostIdAndUserId(USER_ID, USER_ID)).thenReturn(true);

    likeService.existsByPostIdAndUserId(USER_ID, USER_ID);

    verify(likeRepository).existsByPostIdAndUserId(USER_ID, USER_ID);
  }

  @Test
  public void addUser_Should_addUserSuccessfully() {
    likeService.createLike(new Like());

    verify(likeRepository).save(any());
  }

  @Test
  public void getLike_by_PostIdAndUserId_should_return_Like() {
    when(likeRepository.findByUserIdAndPostId(USER_ID, USER_ID)).thenReturn(testLike);
    when(likeService.getByUserIdAndPostId(USER_ID, USER_ID)).thenReturn(testLike);

    likeService.getByUserIdAndPostId(USER_ID, USER_ID);

    verify(likeRepository).findByUserIdAndPostId(USER_ID, USER_ID);
  }

  @Test
  public void addLikeToPost_should_add_Like() {
    Mockito.when(postService.getById(USER_ID)).thenReturn(testPost);
    Mockito.when(principal.getName()).thenReturn("Ivan");
    Mockito.when(userService.getUser("Ivan")).thenReturn(testUser);

    likeService.addLikeToPost(likeModel, principal);

    assertEquals(testPost.getTotalLikes(), 1);
  }

  @Test(expected = EntityAlreadyExist.class)
  public void addLikeToPost_should_throw_exception_whenLikeAlreadyExist() {

    Mockito.when(postService.getById(USER_ID)).thenReturn(testPost);
    Mockito.when(principal.getName()).thenReturn("Ivan");
    Mockito.when(userService.getUser("Ivan")).thenReturn(testUser);
    Mockito.when(likeService.existsByPostIdAndUserId(testPost.getId(), testUser.getId())).thenReturn(true);

    likeService.addLikeToPost(likeModel, principal);
  }

  @Test
  public void addDislikeToPost_should_remove_like() {
    testPost.setTotalLikes(1);

    Mockito.when(postService.getById(USER_ID)).thenReturn(testPost);
    Mockito.when(principal.getName()).thenReturn("Ivan");
    Mockito.when(userService.getUser("Ivan")).thenReturn(testUser);
    Mockito.when(likeService.getByUserIdAndPostId(USER_ID, USER_ID)).thenReturn(testLike);
    Mockito.when(likeService.existsByPostIdAndUserId(testPost.getId(), testUser.getId())).thenReturn(true);

    likeService.dislike(likeModel, principal);

    assertEquals(testPost.getTotalLikes(), 0);
  }

  @Test(expected = EntityNotFoundException.class)
  public void addDislikeToPost_should_throw_exception_whenLikeDoesNotExist() {

    Mockito.when(postService.getById(USER_ID)).thenReturn(testPost);
    Mockito.when(principal.getName()).thenReturn("Ivan");
    Mockito.when(userService.getUser("Ivan")).thenReturn(testUser);
    Mockito.when(likeService.getByUserIdAndPostId(USER_ID, USER_ID)).thenReturn(testLike);
    Mockito.when(likeService.existsByPostIdAndUserId(testPost.getId(), testUser.getId())).thenReturn(false);

    likeService.dislike(likeModel, principal);
  }
}
