package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Request;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityAlreadyExist;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.repositories.RequestRepository;
import com.gaming.project.gamingproject.repositories.UserRepository;
import com.gaming.project.gamingproject.services.contracts.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RequestServiceImplTest {

  @InjectMocks
  private RequestServiceImpl requestService;

  @Mock
  private RequestRepository requestRepository;

  @Mock
  private UserRepository userRepository;

  @Mock
  private Principal principal;

  @Mock
  private UserService userService;

  private final static long FIRST_USER_ID = 1;
  private final static long SECOND_USER_ID = 2;

  private User receiver = new User();
  private User sender = new User();
  private User loggedUser = new User();

  @Test
  public void createRequest_Should_createRequest() {
    requestService.createRequest(new Request());

    verify(requestRepository).save(any());
  }

  @Test
  public void sendRequest_Should_CreateNewRequest() {

    when(userService.findById(FIRST_USER_ID)).thenReturn(receiver);
    when(userService.getUser("Sender")).thenReturn(loggedUser);
    when(principal.getName()).thenReturn("Sender");
    when(requestService.doesRequestExist(loggedUser, receiver)).thenReturn(false);

    requestService.sendRequest(FIRST_USER_ID, principal);

    verify(requestRepository).save(any());
  }

  @Test(expected = EntityAlreadyExist.class)
  public void sendRequest_Should_ThrowExceptionForAlreadyExistingRequest() {

    when(userService.findById(FIRST_USER_ID)).thenReturn(receiver);
    when(userService.getUser("Sender")).thenReturn(loggedUser);
    when(principal.getName()).thenReturn("Sender");
    when(requestService.doesRequestExist(loggedUser, receiver)).thenReturn(true);

    requestService.sendRequest(FIRST_USER_ID, principal);
  }

  @Test
  public void removeFriendRequest_Should_RemoveRequest() {

    when(userService.findById(FIRST_USER_ID)).thenReturn(receiver);
    when(userService.findById(SECOND_USER_ID)).thenReturn(sender);
    when(requestRepository.existsBySenderIdAndReceiverId(sender.getId(), receiver.getId())).thenReturn(true);
    when(requestRepository.existsBySenderIdAndReceiverId(receiver.getId(), sender.getId())).thenReturn(true);

    requestService.removeFriendRequest(FIRST_USER_ID, SECOND_USER_ID);

    verify(requestRepository).deleteBySenderAndReceiver(sender, receiver);
  }

  @Test(expected = EntityNotFoundException.class)
  public void removeFriendRequest_Should_ThrowExceptionIfNoReqeust() {

    when(userService.findById(FIRST_USER_ID)).thenReturn(receiver);
    when(userService.findById(SECOND_USER_ID)).thenReturn(sender);
    when(requestRepository.existsBySenderIdAndReceiverId(sender.getId(), receiver.getId())).thenReturn(false);
    when(requestRepository.existsBySenderIdAndReceiverId(receiver.getId(), sender.getId())).thenReturn(false);

    requestService.removeFriendRequest(FIRST_USER_ID, SECOND_USER_ID);
  }

}
