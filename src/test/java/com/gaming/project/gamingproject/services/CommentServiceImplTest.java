package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.repositories.CommentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

  @InjectMocks
  private CommentServiceImpl commentService;

  @Mock
  private CommentRepository commentRepository;

  private Comment comment = new Comment();

  @Test
  public void createComment_Should_createComment() {
    commentService.createComment(new Comment());

    verify(commentRepository).save(any());
  }

  @Test
  public void deleteCommentById_should_deleteComment() {
    when(commentRepository.findById(1L)).thenReturn(java.util.Optional.ofNullable(comment));

    commentService.deleteCommentById(1L);

    verify(commentRepository).delete(comment);
  }
}
