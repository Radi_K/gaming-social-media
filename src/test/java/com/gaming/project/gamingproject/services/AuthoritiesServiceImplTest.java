package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.repositories.AuthoritiesRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthoritiesServiceImplTest {

  @InjectMocks
  private AuthoritiesServiceImpl authoritiesService;

  @Mock
  private AuthoritiesRepository authoritiesRepository;

  @Test
  public void doesAuthorityExist_should_check_ifExist() {

    when(authoritiesService.doesAuthorityExist(any())).thenReturn(true);

    authoritiesService.doesAuthorityExist("Michael");

    verify(authoritiesRepository).existsById("Michael");
  }

  @Test
  public void deleteAuthority_should_deleteAuthority() {

    authoritiesService.deleteAuthority("Michael");

    verify(authoritiesRepository).deleteById("Michael");
  }
}
