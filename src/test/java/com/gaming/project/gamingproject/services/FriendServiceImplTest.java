package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Friend;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.repositories.FriendRepository;
import com.gaming.project.gamingproject.services.contracts.RequestService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FriendServiceImplTest {

  @InjectMocks
  private FriendServiceImpl friendService;

  @Mock
  private FriendRepository friendRepository;

  @Mock
  private Principal principal;

  @Mock
  private UserService userService;

  @Mock
  private RequestService requestService;

  @Mock
  private LoginServiceImpl loginService;

  private User testSender = new User();
  private User testReceiver = new User();
  private User loggedUser = new User();
  private Friend friendShip = new Friend(testReceiver, loggedUser);

  @Before
  public void setUp() {
    testSender.setId(1L);
    testReceiver.setId(2L);
  }

  @Test
  public void findByUsername_should_return_correctLogin() {
    friendService.createFriendship(new Friend());

    verify(friendRepository).save(any());
  }

  @Test
  public void doesFriendshipExist_should_check_ifExist() {

    when(friendService.doesFriendshipExist(testSender, testReceiver)).thenReturn(true);

    friendService.doesFriendshipExist(testSender, testReceiver);

    verify(friendRepository).existsBySenderIdAndReceiverId(1L, 2L);
  }

  @Test
  public void deleteFriendShip_should_deleteFriendship() {
    testSender.setLogin(new Login());
    testReceiver.setLogin(new Login());
    friendService.deleteFriendShip(testSender, testReceiver);

    verify(friendRepository).deleteBySenderAndReceiver(any(), any());
  }

  @Test
  public void findFriendsByUserName_Should_return_listOfFriends() {
    List<Friend> friends = new ArrayList<>();
    friends.add(new Friend());
    friends.add(new Friend());

    when(friendRepository.findAllByReceiverLoginUsername(any())).thenReturn(friends);

    assertEquals(2, friendService.findFriendsByUserName(any()).size());
  }

  @Test
  public void addAsFriend_Should_AcceptTheRequest() {

    when(userService.findById(1L)).thenReturn(testReceiver);
    when(requestService.doesRequestExist(any(), any())).thenReturn(true);

    friendService.addAsFriend(1L, principal);

    verify(friendRepository).save(any());
  }

  @Test(expected = EntityNotFoundException.class)
  public void addAsFriend_Should_ThrowExceptionIfRequestDoNotExist() {
    when(userService.findById(1L)).thenReturn(testReceiver);
    when(requestService.doesRequestExist(any(), any())).thenReturn(false);

    friendService.addAsFriend(1L, principal);
  }

  @Test
  public void removeAsFriend_Should_RemoveFriend() {
    loggedUser.setLogin(new Login());
    loggedUser.getLogin().setUsername("Sender");
    testReceiver.setLogin(new Login());
    loggedUser.getLogin().setUsername("Receiver");
    when(userService.findById(1L)).thenReturn(testReceiver);
    when(principal.getName()).thenReturn("Sender");
    when(userService.getUser("Sender")).thenReturn(loggedUser);
    when(friendService.doesFriendshipExist(loggedUser, testReceiver)).thenReturn(true);

    friendService.removeFromFriends(1L, principal);

    verify(friendRepository).deleteBySenderAndReceiver(loggedUser, testReceiver);
  }

  @Test
  public void removeAsFriend_Should_RemoveFriend2() {
    loggedUser.setLogin(new Login());
    loggedUser.getLogin().setUsername("Sender");
    testReceiver.setLogin(new Login());
    loggedUser.getLogin().setUsername("Receiver");
    when(userService.findById(1L)).thenReturn(testReceiver);
    when(principal.getName()).thenReturn("Sender");
    when(userService.getUser("Sender")).thenReturn(loggedUser);
    when(friendService.doesFriendshipExist(testReceiver, loggedUser)).thenReturn(true);

    friendService.removeFromFriends(1L, principal);

    verify(friendRepository).deleteBySenderAndReceiver(testReceiver, loggedUser);
  }

  @Test(expected = EntityNotFoundException.class)
  public void removeAsFriend_Should_ThrowExceptionIfFriendshipDoNotExist() {
    loggedUser.setLogin(new Login());
    loggedUser.getLogin().setUsername("Sender");
    testReceiver.setLogin(new Login());
    loggedUser.getLogin().setUsername("Receiver");
    when(userService.findById(1L)).thenReturn(testReceiver);
    when(principal.getName()).thenReturn("Sender");
    when(userService.getUser("Sender")).thenReturn(loggedUser);
    when(friendService.doesFriendshipExist(testReceiver, loggedUser)).thenReturn(false);

    friendService.removeFromFriends(1L, principal);
  }
}
