package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.entities.PostAccessibility;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.CommentModel;
import com.gaming.project.gamingproject.models.PostModel;
import com.gaming.project.gamingproject.models.PostVisualisationModel;
import com.gaming.project.gamingproject.repositories.CommentRepository;
import com.gaming.project.gamingproject.repositories.LikeCommentRepository;
import com.gaming.project.gamingproject.repositories.PostRepository;
import com.gaming.project.gamingproject.repositories.UserRepository;
import com.gaming.project.gamingproject.services.contracts.LikeCommentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceImplTest {

  @InjectMocks
  private PostServiceImpl postService;

  @Mock
  private ModelMapper modelMapper;

  @Mock
  private LikeCommentService likeCommentService;

  @Mock
  private Principal principal;

  @Mock
  private UserRepository userRepository;

  @Mock
  private CommentRepository commentRepository;

  @Mock
  private LikeCommentRepository likeCommentRepository;

  @Mock
  private PostRepository postRepository;

  private final static long USER_ID = 1;

  private CommentModel commentModel = new CommentModel();
  private PostModel postModel = new PostModel();
  private User loggedUser = new User();
  private Post post = new Post();
  private List<PostVisualisationModel> postVisualisationModelList = new ArrayList<>();
  private Page<PostVisualisationModel> page = new PageImpl<>(postVisualisationModelList);

  @Before
  public void setUp() {
    postVisualisationModelList.add(new PostVisualisationModel());
    postVisualisationModelList.add(new PostVisualisationModel());

    modelMapper = new ModelMapper();

    loggedUser.setPosts(new ArrayList<>());
    loggedUser.getPosts().add(new Post());
    loggedUser.setFirstName("Ivan");
    loggedUser.setLastName("Ivanov");
    loggedUser.setId(USER_ID);

    post.setId(USER_ID);
    post.setPostAccessibility(PostAccessibility.PUBLIC);

    postModel.setUserPicture("picture7");
    postModel.setUserId(USER_ID);
    postModel.setPostAccessibility("PUBLIC");
    postModel.setText("asdasd");

    commentModel.setPicture("picture7");
    commentModel.setPostId(USER_ID);
    commentModel.setText("asd");

  }

  @Test
  public void createPosts_Should_return_newPost() {
    when(postService.createPost(post)).thenReturn(post);
    assertEquals(post, postService.createPost(post));
    verify(postRepository).save(any());
  }

  @Test
  public void getById_Should_return_correctPost() {
    when(postRepository.findById(USER_ID)).thenReturn(java.util.Optional.ofNullable(post));
    Post postResult = postService.getById(USER_ID);

    assertEquals(Long.valueOf(1), postResult.getId());
  }

  @Test
  public void deletePost_Should_remove_post() {

    when(postRepository.existsById(USER_ID)).thenReturn(true);
    when(postRepository.findById(USER_ID)).thenReturn(java.util.Optional.ofNullable(post));

    postService.deletePost(USER_ID);

    verify(postRepository).delete(post);
  }

  @Test(expected = EntityNotFoundException.class)
  public void deletePost_Should_throw_exception_when_postIsMissing() {

    when(postRepository.existsById(USER_ID)).thenReturn(true);

    postService.deletePost(USER_ID);
  }
}
