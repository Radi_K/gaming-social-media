package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.entities.LikeComment;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityAlreadyExist;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.LikeCommentModel;
import com.gaming.project.gamingproject.repositories.CommentRepository;
import com.gaming.project.gamingproject.repositories.LikeCommentRepository;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LikeCommentServiceImplTest {

  @InjectMocks
  private LikeCommentServiceImpl likeCommentService;

  @Mock
  private LikeCommentRepository likeCommentRepository;

  @Mock
  private UserService userService;

  @Mock
  private CommentRepository commentRepository;

  @Mock
  private Principal principal;

  @Mock
  private CommentService commentService;
  
  private final static long USER_ID = 1;

  private LikeCommentModel likeCommentModel = new LikeCommentModel();
  private LikeComment likeComment = new LikeComment();
  private User user = new User();
  private Comment comment = new Comment();

  @Before
  public void setUp() {
    likeCommentModel.setCommentId(USER_ID);
    likeCommentModel.setUserId(USER_ID);
    user.setId(USER_ID);
    user.setLogin(new Login("Ivan", "asd", true));
    comment.setId(USER_ID);
    likeComment.setId(USER_ID);
  }

  @Test
  public void existsByPostIdAndUserId_Should_returnInformationAboutExisting() {
    when(likeCommentService.existsByCommentIdAndUserId( USER_ID,  USER_ID)).thenReturn(true);

    likeCommentService.existsByCommentIdAndUserId( USER_ID,  USER_ID);

    verify(likeCommentRepository).existsByCommentIdAndUserId( USER_ID,  USER_ID);
  }

  @Test
  public void createLikeComment_Should_return_likeToComment() {
    likeCommentService.createLikeComment(likeComment);

    verify(likeCommentRepository).save(any());
  }

  @Test
  public void getLike_by_PostIdAndUserId_should_return_Like() {
    when(likeCommentRepository.findByUserIdAndCommentId( USER_ID,  USER_ID)).thenReturn(likeComment);

    likeCommentService.getByUserIdAndCommentId( USER_ID,  USER_ID);

    verify(likeCommentRepository).findByUserIdAndCommentId( USER_ID,  USER_ID);
  }

  @Test
  public void addLikeToComment_should_return_LikeToComment() {

    when(principal.getName()).thenReturn("Ivan");
    when(userService.getUser("Ivan")).thenReturn(user);
    when(commentService.getById( USER_ID)).thenReturn(comment);
    when(likeCommentService.existsByCommentIdAndUserId( USER_ID,  USER_ID)).thenReturn(false);

    likeCommentService.addLikeToComment(likeCommentModel, principal);

    assertEquals(comment.getTotalLikes(), 1);
  }

  @Test(expected = EntityAlreadyExist.class)
  public void addLikeToComment_should_throw_exception_when_LikeDoesNotExist() {

    when(principal.getName()).thenReturn("Ivan");
    when(userService.getUser("Ivan")).thenReturn(user);
    when(likeCommentService.existsByCommentIdAndUserId( USER_ID,  USER_ID)).thenReturn(true);
    when(commentService.getById( USER_ID)).thenReturn(comment);

    likeCommentService.addLikeToComment(likeCommentModel, principal);

    verify(commentRepository).save(comment);
  }

  @Test
  public void addDislikeToComment_should_return_removeLikeToComment() {
    comment.setTotalLikes(1);

    when(principal.getName()).thenReturn("Ivan");
    when(userService.getUser("Ivan")).thenReturn(user);
    when(likeCommentService.getByUserIdAndCommentId( USER_ID,  USER_ID)).thenReturn(likeComment);
    when(likeCommentService.existsByCommentIdAndUserId( USER_ID,  USER_ID)).thenReturn(true);
    when(commentService.getById(USER_ID)).thenReturn(comment);

    likeCommentService.dislike(likeCommentModel, principal);

    assertEquals(comment.getTotalLikes(), 0);
  }

  @Test(expected = EntityNotFoundException.class)
  public void addDislikeToComment_should_throw_exception_when_LikeDoesNotExist() {

    when(principal.getName()).thenReturn("Ivan");
    when(userService.getUser("Ivan")).thenReturn(user);
    when(likeCommentService.existsByCommentIdAndUserId( USER_ID,  USER_ID)).thenReturn(false);
    when(commentService.getById( USER_ID)).thenReturn(comment);

    likeCommentService.dislike(likeCommentModel, principal);

    verify(commentRepository).save(comment);
  }

}
