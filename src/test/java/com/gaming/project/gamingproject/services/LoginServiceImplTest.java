package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.RoleType;
import com.gaming.project.gamingproject.entities.UserCountries;
import com.gaming.project.gamingproject.exceptions.EntityAlreadyExist;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.LoginModel;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.repositories.LoginRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceImplTest {

  @InjectMocks
  private LoginServiceImpl loginService;

  @Mock
  private LoginRepository loginRepository;

  @Mock
  private UserDetailsManager userDetailsManager;

  @Mock
  private PasswordEncoder passwordEncoder;

  private LoginModel loginModel = new LoginModel();
  private RegisterUserModel registerUserModel = new RegisterUserModel();
  private Login login = new Login();

  @Test
  public void findByUsername_should_return_correctLogin() {
    login.setUsername("test");
    when(loginRepository.findById(login.getUsername())).thenReturn(java.util.Optional.of(login));

    assertEquals(loginService.findByUsername("test"), login);
    verify(loginRepository).findById(any());
  }

  @Test(expected = EntityNotFoundException.class)
  public void findByUsername_should_throw_exceptionWhenLoginDoesNotExist() {
    login.setUsername("test");

    loginService.findByUsername("notFound");
  }

  @Test
  public void createLogin_should_createLogin() {

    registerUserModel.setLoginUsername("test");
    registerUserModel.setLoginPassword("123321");
    registerUserModel.setPasswordConfirmation("123321");
    registerUserModel.setEmail("test.mail@mail.mm");
    registerUserModel.setFirstName("TestUser");
    registerUserModel.setLastName("TestUser");
    registerUserModel.setAge(20);
    registerUserModel.setGender(GenderType.OTHER);
    registerUserModel.setCountry(UserCountries.Bulgaria.toString());

    List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(
            RoleType.ROLE_USER.toString());

    User regUser = new User(
            registerUserModel.getLoginUsername(),
            "123321",
            false,
            true,
            true,
            true,
            authorities);

    when(loginRepository.existsById(any())).thenReturn(false);
    when(passwordEncoder.encode(registerUserModel.getLoginPassword())).thenReturn("123321");

    loginService.createLogin(registerUserModel);

    verify(userDetailsManager).createUser(regUser);
  }

  @Test(expected = EntityAlreadyExist.class)
  public void createLogin_should_ThrowExceptionIfUserAlreadyExist() {
    when(loginRepository.existsById(registerUserModel.getLoginUsername())).thenReturn(true);

    loginService.createLogin(registerUserModel);
  }

  @Test
  public void save_should_createNewLogin() {
    loginService.save(login);

    verify(loginRepository).save(login);
  }
}
