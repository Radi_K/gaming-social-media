package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.models.CommentEditModel;
import com.gaming.project.gamingproject.models.CommentModel;
import com.gaming.project.gamingproject.models.CommentVisualizationModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

/**
 * Service for comments providing different operations
 */

public interface CommentService {

  Comment createComment(Comment comment);

  Comment getById(Long id);

  CommentVisualizationModel convertComment(Comment comment);

  Page<CommentVisualizationModel> findCommentsForUser(Pageable pageable,
                                                      Long userId);

  void deleteCommentById(Long commentId);

  CommentEditModel editComment(CommentEditModel commentModel);

  @Transactional
  CommentVisualizationModel addCommentToPost(
          CommentModel commentModel, Principal principal);
}
