package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.models.RegisterUserModel;

/**
 * Service for providing part of the registration
 */

public interface LoginService {

  Login findByUsername(String username);

  void createLogin(RegisterUserModel registerUserModel);

  void save(Login login);
}
