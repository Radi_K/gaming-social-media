package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.models.PostEditModel;
import com.gaming.project.gamingproject.models.PostModel;
import com.gaming.project.gamingproject.models.PostVisualisationModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;

/**
 * Service providing all operations for post.
 * Operate with elements from database
 */

public interface PostService {

  Post createPost(Post post);

  Post getById(Long id);

  void deletePost(Long postId);

  PostEditModel editPost(PostEditModel postId);

  Page<PostVisualisationModel> findTop5PostsByLikes(Pageable pageable,
                                                    Principal principal);

  Page<PostVisualisationModel> getPostForUserPage(Pageable pageable,
                                                  Principal principal);

  Page<PostVisualisationModel> findPostsByUsername(
          Pageable pageable, String userLoginUsername, Principal principal);

  Page<PostVisualisationModel> postsPagination(Pageable pageable,
                                               Principal principal);

  PostVisualisationModel addPost(PostModel postModel, Principal principal);

  Page<PostVisualisationModel> findPostsForUser(Pageable pageable, Long id);

  Page<PostVisualisationModel> findAllPublicPosts(Pageable pageable);
}
