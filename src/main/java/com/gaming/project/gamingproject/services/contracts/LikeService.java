package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Like;
import com.gaming.project.gamingproject.models.LikeModel;

import java.security.Principal;

/**
 * Like service with operations operating on database elements
 */

public interface LikeService {

  boolean existsByPostIdAndUserId(Long postId, Long userId);

  void createLike(Like like);

  Like getByUserIdAndPostId(Long userId, Long beerId);

  Integer addLikeToPost(LikeModel likeModel, Principal principal);

  Integer dislike(LikeModel likeModel, Principal principal);
}
