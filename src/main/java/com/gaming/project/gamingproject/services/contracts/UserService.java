package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.models.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

import java.security.Principal;
import java.util.List;

/**
 * This service provides operations for user/users.
 */

public interface UserService {

  void addUser(User user);

  void createUser(RegisterUserModel registerUserModel);

  List<User> findAllByGenderType(GenderType genderType);

  Page<UserModel> userPagination(Pageable pageable, Principal principal);

  UserModel findByIdModel(Long userId);

  UserModel editUser(UserModel userModel);

  User getUser(String userName);

  User findById(Long userId);

  Long countUsers();

  List<UserModel> getAllUsersByFirstNameOrLastName(String name,
                                                   String lastName);

  void deleteUserById(Long userId);

  User findByEmailIgnoreCase(String email);

  ConfirmationToken createUserAndToken(RegisterUserModel user);

  String getUserRole(Authentication principal);

  User retrieveUser(ConfirmationToken token);

  void userNewPassword(User user);
}
