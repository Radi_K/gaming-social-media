package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.entities.LikeComment;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityAlreadyExist;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.LikeCommentModel;
import com.gaming.project.gamingproject.repositories.LikeCommentRepository;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import com.gaming.project.gamingproject.services.contracts.LikeCommentService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

@Service
@RequiredArgsConstructor
public class LikeCommentServiceImpl implements LikeCommentService {

  private final LikeCommentRepository likeCommentRepository;
  private final CommentService commentService;
  private final UserService userService;

  @Override
  public boolean existsByCommentIdAndUserId(Long commentId, Long userId) {
    return likeCommentRepository.existsByCommentIdAndUserId(commentId, userId);
  }

  @Override
  public void createLikeComment(LikeComment likeComment) {
    likeCommentRepository.save(likeComment);
  }

  @Override
  public LikeComment getByUserIdAndCommentId(Long userId, Long commentId) {
    return likeCommentRepository.findByUserIdAndCommentId(userId, commentId);
  }

  @Override
  public Integer addLikeToComment(LikeCommentModel likeCommentModel,
                                  Principal principal) {
    Comment comment = commentService.getById(likeCommentModel.getCommentId());
    User user = userService.getUser(principal.getName());

    if (existsByCommentIdAndUserId(comment.getId(), user.getId())) {
      throw new EntityAlreadyExist("Entity already exist!");
    }

    LikeComment likeComment = new LikeComment(comment, user);
    createLikeComment(likeComment);
    comment.setTotalLikes(comment.getTotalLikes() + 1);
    commentService.createComment(comment);
    return comment.getTotalLikes();
  }

  @Override
  public Integer dislike(LikeCommentModel likeCommentModel,
                         Principal principal) {
    Comment comment = commentService.getById(likeCommentModel.getCommentId());
    User user = userService.getUser(principal.getName());

    if (!existsByCommentIdAndUserId(comment.getId(), user.getId())) {
      throw new EntityNotFoundException("Entity not found exception!");
    }
    LikeComment likeComment =
            getByUserIdAndCommentId(user.getId(),
                    likeCommentModel.getCommentId());
    likeCommentRepository.delete(likeComment);
    comment.setTotalLikes(comment.getTotalLikes() - 1);
    commentService.createComment(comment);
    return comment.getTotalLikes();
  }

  @Transactional
  @Override
  public void deleteCommentLikesByCommentId(Long commentId) {
    likeCommentRepository.deleteAllByCommentId(commentId);
  }

  @Transactional
  @Override
  public void deleteCommentLikesByUserId(Long userId) {
    likeCommentRepository.deleteAllByUserId(userId);
  }

}
