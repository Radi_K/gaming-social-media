package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.Friend;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.FriendVisualisationModel;
import com.gaming.project.gamingproject.models.UserModel;
import com.gaming.project.gamingproject.models.UserRequestCheckModel;

import java.security.Principal;
import java.util.List;

/**
 * Service with operations on friendship between two users
 */

public interface FriendService {

  void createFriendship(Friend friend);

  boolean doesFriendshipExist(User sender, User receiver);

  void deleteFriendShip(User sender, User receiver);

  List<FriendVisualisationModel> convertListFriend(List<Friend> friends);

  List<Friend> findFriendsByUserName(String userLoginUsername);

  void addAsFriend(Long wantedUserId, Principal principal);

  void removeFromFriends(Long wantedUserId, Principal principal);

  List<UserModel> checkUsersForFriendshipOrRequest(List<UserModel> usersToCheck, Principal principal);

  UserRequestCheckModel checkUserFriendship(Long userToCheckId, Principal principal);
}
