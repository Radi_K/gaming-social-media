package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.RoleType;
import com.gaming.project.gamingproject.exceptions.EntityAlreadyExist;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.LoginModel;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.repositories.LoginRepository;
import com.gaming.project.gamingproject.services.contracts.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

  private final LoginRepository loginRepository;
  private final UserDetailsManager userDetailsManager;
  private final PasswordEncoder passwordEncoder;

  @Override
  public Login findByUsername(String username) {
    return loginRepository.findById(username).orElseThrow(
            () -> new EntityNotFoundException("Login with this username not found!"));
  }

  @Override
  public void createLogin(RegisterUserModel registerUserModel) {
    if (loginRepository.existsById(registerUserModel.getLoginUsername())) {
      throw new EntityAlreadyExist("User with this username already exist!");
    }

    LoginModel loginModel = new LoginModel();
    loginModel.setUsername(registerUserModel.getLoginUsername());
    loginModel.setPassword(registerUserModel.getLoginPassword());
    loginModel.setPasswordConfirmation(registerUserModel.getPasswordConfirmation());
    List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(
            RoleType.ROLE_USER.toString());

    User newUser =
            (User) User.builder()
                    .username(loginModel.getUsername())
                    .password(passwordEncoder.encode(loginModel.getPassword()))
                    .disabled(true)
                    .accountExpired(true)
                    .credentialsExpired(true)
                    .accountLocked(true)
                    .authorities(authorities)
                    .build();

    userDetailsManager.createUser(newUser);
  }

  public void save(Login login) {
    loginRepository.save(login);
  }
}
