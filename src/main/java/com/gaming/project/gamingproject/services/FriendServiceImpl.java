package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Friend;
import com.gaming.project.gamingproject.entities.RequestType;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.FriendVisualisationModel;
import com.gaming.project.gamingproject.models.UserModel;
import com.gaming.project.gamingproject.models.UserRequestCheckModel;
import com.gaming.project.gamingproject.repositories.FriendRepository;
import com.gaming.project.gamingproject.services.contracts.FriendService;
import com.gaming.project.gamingproject.services.contracts.RequestService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class FriendServiceImpl implements FriendService {

  private final FriendRepository friendRepository;
  private final RequestService requestService;
  private final UserService userService;
  private final ModelMapper mapper;

  @Override
  public void createFriendship(Friend friend) {
    friendRepository.save(friend);
  }

  @Override
  public boolean doesFriendshipExist(User sender, User receiver) {
    return friendRepository.existsBySenderIdAndReceiverId(sender.getId(),
            receiver.getId());
  }

  @Transactional
  @Override
  public void deleteFriendShip(User sender, User receiver) {
    friendRepository.deleteBySenderAndReceiver(sender, receiver);
    log.warn("Friendship deleted between - {}",
            sender.getLogin().getUsername() + " - " + receiver.getLogin().getUsername());
  }

  @Override
  public List<Friend> findFriendsByUserName(String userFirstName) {
    return friendRepository.findAllByReceiverLoginUsername(userFirstName);
  }

  @Override
  public List<FriendVisualisationModel> convertListFriend(List<Friend> friends) {
    return friends.stream().map(this::convertFriend).collect(Collectors.toList());
  }

  @Override
  public void addAsFriend(Long wantedUserToAddId, Principal principal) {
    User sender = userService.findById(wantedUserToAddId);
    User receiver = userService.getUser(principal.getName());
    if (!requestService.doesRequestExist(sender, receiver)) {
      throw new EntityNotFoundException("Request does not exist!");
    }
    requestService.deleteRequest(sender, receiver);
    Friend friendShip = new Friend(receiver, sender);
    createFriendship(friendShip);
  }

  @Override
  public void removeFromFriends(Long wantedUserToRemoveId, Principal principal) {
    User loggedUser = userService.getUser(principal.getName());
    User userEndedFriendship = userService.findById(wantedUserToRemoveId);
    if (doesFriendshipExist(loggedUser, userEndedFriendship)) {
      deleteFriendShip(loggedUser, userEndedFriendship);
    } else if (doesFriendshipExist(userEndedFriendship, loggedUser)) {
      deleteFriendShip(userEndedFriendship, loggedUser);
    } else {
      throw new EntityNotFoundException("Friendship does not exist!");
    }
  }

  @Override
  public List<UserModel> checkUsersForFriendshipOrRequest(List<UserModel> usersToCheck, Principal principal) {
    mapper.getConfiguration().setAmbiguityIgnored(true);
    User loggedUser = userService.getUser(principal.getName());
    return usersToCheck.stream()
            .map(user -> convertAndCheckUser(user, loggedUser))
            .collect(Collectors.toList());
  }

  @Override
  public UserRequestCheckModel checkUserFriendship(Long userToCheckId, Principal principal) {
    User loggedUser = userService.getUser(principal.getName());
    return convertToEntityFriendshipCheck(userService.findById(userToCheckId), loggedUser);
  }

  private UserRequestCheckModel convertToEntityFriendshipCheck(User userToCheck, User principal) {
    UserRequestCheckModel model = new UserRequestCheckModel();
    if (requestService.doesRequestExist(principal, userToCheck) || requestService.doesRequestExist(userToCheck,
            principal)) {
      model.setRequestType(RequestType.PENDING);
    } else if (doesFriendshipExist(principal, userToCheck)
            || doesFriendshipExist(userToCheck, principal)) {
      model.setRequestType(RequestType.ACCEPTED);
    } else {
      model.setRequestType(RequestType.EMPTY);
    }
    return model;
  }

  private UserModel convertAndCheckUser(UserModel userModel, User loggedUser) {
    User user = mapper.map(userModel, User.class);
    UserModel model = mapper.map(user, UserModel.class);
    if (shouldSetFriend(loggedUser, user)) {
      model.setHasFriend(true);
    }
    return model;
  }

  private boolean shouldSetFriend(User loggedUser, User user) {
    return (requestService.doesRequestExist(user, loggedUser) || requestService.doesRequestExist(loggedUser, user))
            || doesFriendshipExist(user, loggedUser) || doesFriendshipExist(loggedUser, user);
  }


  private FriendVisualisationModel convertFriend(Friend friend) {
    return mapper.map(friend, FriendVisualisationModel.class);
  }
}
