package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.LikeComment;
import com.gaming.project.gamingproject.models.LikeCommentModel;

import java.security.Principal;

/**
 * Service with operations which operate on elements from database
 */

public interface LikeCommentService {

  boolean existsByCommentIdAndUserId(Long commentId, Long userId);

  void createLikeComment(LikeComment likeComment);

  LikeComment getByUserIdAndCommentId(Long userId, Long commentId);

  Integer addLikeToComment(LikeCommentModel likeCommentModel,
                           Principal principal);

  Integer dislike(LikeCommentModel likeCommentModel, Principal principal);

  void deleteCommentLikesByUserId(Long userId);

  void deleteCommentLikesByCommentId(Long commentId);
}
