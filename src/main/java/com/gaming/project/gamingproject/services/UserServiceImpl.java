package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.Login;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.models.UserModel;
import com.gaming.project.gamingproject.repositories.UserRepository;
import com.gaming.project.gamingproject.services.contracts.AuthoritiesService;
import com.gaming.project.gamingproject.services.contracts.ConfirmationTokenService;
import com.gaming.project.gamingproject.services.contracts.LoginService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Setter
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;
  private final LoginService loginService;
  private final AuthoritiesService authoritiesService;
  private final ConfirmationTokenService tokenService;
  private final BCryptPasswordEncoder encoder;
  private final ModelMapper mapper;

  @Override
  public void addUser(User user) {
    userRepository.save(user);
  }

  @Override
  public void createUser(RegisterUserModel registerUserModel) {
    loginService.createLogin(registerUserModel);
    Login login =
            loginService.findByUsername(registerUserModel.getLoginUsername());
    User user = new User();
    user.setFirstName(registerUserModel.getFirstName());
    user.setLastName(registerUserModel.getLastName());
    user.setEmail(registerUserModel.getEmail());
    user.setAge(registerUserModel.getAge());
    user.setGender(registerUserModel.getGender());
    user.setCountry(registerUserModel.getCountry());
    pictureCheck(login, user, registerUserModel.getGender(), registerUserModel);
    login.setEnabled(true);
    loginService.save(login);
    userRepository.save(user);
  }

  @Override
  public List<User> findAllByGenderType(GenderType genderType) {
    return userRepository.findByGenderEquals(genderType);
  }

  @Override
  public Page<UserModel> userPagination(Pageable pageable,
                                        Principal principal) {
    return userRepository
            .findAll(pageable, principal.getName())
            .map(this::convertDto);
  }

  @Override
  public UserModel findByIdModel(Long userId) {
    return convertDto(userRepository.findById(userId)
            .orElseThrow(() -> new EntityNotFoundException("User not found!")));
  }

  @Override
  public User findById(Long userId) {
    return userRepository.findById(userId).orElseThrow(() ->
            new EntityNotFoundException("User does not exist!"));
  }

  @Override
  public Long countUsers() {
    return userRepository.count();
  }

  @Override
  public List<UserModel> getAllUsersByFirstNameOrLastName(String firstName,
                                                          String lastName) {
    return convertListToDto(
            userRepository.findByFirstNameStartingWithOrLastNameStartingWith(firstName, lastName));
  }

  @Override
  public User findByEmailIgnoreCase(String email) {
    return userRepository.findByEmailIgnoreCase(email);
  }

  @Override
  public ConfirmationToken createUserAndToken(RegisterUserModel userModel) {
    loginService.createLogin(userModel);
    Login login = loginService.findByUsername(userModel.getLoginUsername());
    ConfirmationToken confirmationToken =
            new ConfirmationToken(convertFromRegisterModelToEntity(userModel,
                    login));
    tokenService.createToken(confirmationToken);

    return confirmationToken;
  }

  @Override
  public String getUserRole(Authentication principal) {
    String userRole = principal.getAuthorities().toString();
    int firstDelimiter = userRole.indexOf("_");
    userRole = userRole.substring(firstDelimiter + 1, userRole.length() - 1);
    return userRole;
  }

  @Transactional
  @Override
  public void deleteUserById(Long userId) {
    if (!userRepository.existsById(userId)) {
      throw new EntityNotFoundException("User does not exist!");
    }
    User user = findById(userId);
    final String username = user.getLogin().getUsername();
    authoritiesService.deleteAuthority(username);
    tokenService.deleteTokenByUserId(userId);
    userRepository.delete(user);
    log.warn("Deleted user - {}", username);
  }

  @Override
  public UserModel editUser(UserModel userModel) {
    User user = findById(userModel.getId());
    user.setFirstName(userModel.getFirstName());
    user.setLastName(userModel.getLastName());
    user.setEmail(userModel.getEmail());
    user.setAge(userModel.getAge());
    user.setGender(GenderType.valueOf(userModel.getGender()));
    user.setCountry(userModel.getCountry());
    log.warn("Edited user - {}", user.getLogin().getUsername());
    return convertDto(userRepository.save(user));
  }

  @Override
  public User getUser(String username) {
    if (!userRepository.existsByLoginUsername(username)) {
      throw new EntityNotFoundException("User does not exist!");
    }
    return userRepository.findFirstByLoginUsername(username);
  }

  @Override
  public User retrieveUser(ConfirmationToken token) {
    User tokenUser = findByEmailIgnoreCase(token.getUser().getEmail());
    tokenUser.getLogin().setEnabled(true);
    addUser(tokenUser);
    return tokenUser;
  }

  @Override
  public void userNewPassword(User user) {
    User tokenUser = findByEmailIgnoreCase(user.getEmail());
    tokenUser.getLogin().setPassword(encoder.encode(user.getLogin().getPassword()));
    addUser(tokenUser);
  }

  private List<UserModel> convertListToDto(List<User> users) {
    return users.stream().map(this::convertDto).collect(Collectors.toList());
  }

  private UserModel convertDto(User user) {
    return mapper.map(user, UserModel.class);
  }

  private User convertFromRegisterModelToEntity(RegisterUserModel model,
                                                Login login) {
    User user = mapper.map(model, User.class);
    pictureCheck(login, user, user.getGender(), model);
    return user;
  }

  private void pictureCheck(Login login, User user, GenderType gender, RegisterUserModel registerUserModel) {
    user.setLogin(login);
    user.setRegisterTime(LocalDateTime.now());
    if (gender.equals(GenderType.MALE)) {
      user.setPicture("avatar7.png");
    } else if (gender.equals(GenderType.OTHER)) {
      user.setPicture("avatar6.png");
    } else {
      user.setPicture("avatar3.png");
    }
  }
}
