package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Request;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityAlreadyExist;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.RequestVisualisationModel;
import com.gaming.project.gamingproject.repositories.RequestRepository;
import com.gaming.project.gamingproject.services.contracts.RequestService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

  private final RequestRepository requestRepository;
  private final UserService userService;

  @Override
  public void createRequest(Request request) {
    requestRepository.save(request);
  }

  @Override
  public boolean doesRequestExist(User sender, User receiver) {
    return requestRepository.existsBySenderIdAndReceiverId(sender.getId(),
            receiver.getId());
  }

  @Transactional
  @Override
  public void deleteRequest(User sender, User receiver) {
    requestRepository.deleteBySenderAndReceiver(sender, receiver);
  }

  @Override
  public Page<RequestVisualisationModel> getAllRequestsByUserName(
          Principal principal, Pageable pageable) {
    return requestRepository
            .findAllByReceiverLoginUsername(principal.getName(), pageable)
            .map(this::convertRequest);
  }

  @Override
  public void sendRequest(Long userReceivingRequestId, Principal principal) {
    User receiver = userService.findById(userReceivingRequestId);
    User sender = userService.getUser(principal.getName());
    if (doesRequestExist(sender, receiver)) {
      throw new EntityAlreadyExist("Request already exist");
    }
    createRequest(new Request(receiver, sender));
  }

  @Override
  public void removeFriendRequest(Long firstConnectedUserId, Long secondConnectedUserId) {
    User firstConnectedUser = userService.findById(firstConnectedUserId);
    User secondConnectedUser = userService.findById(secondConnectedUserId);

    if (!doesRequestExist(firstConnectedUser, secondConnectedUser) && !doesRequestExist(secondConnectedUser,
            firstConnectedUser)) {
      throw new EntityNotFoundException("No such request.");
    }
    deleteRequest(secondConnectedUser, firstConnectedUser);
  }

  private RequestVisualisationModel convertRequest(Request request) {
    ModelMapper mapper = new ModelMapper();
    return mapper.map(request, RequestVisualisationModel.class);
  }
}
