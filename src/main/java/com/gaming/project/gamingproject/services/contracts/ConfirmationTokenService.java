package com.gaming.project.gamingproject.services.contracts;

import com.gaming.project.gamingproject.entities.ConfirmationToken;

/**
 * Service with operations for tokens generated for verification
 */

public interface ConfirmationTokenService {

  void createToken(ConfirmationToken confirmationToken);

  ConfirmationToken findByConfirmationToken(String confirmationToken);

  void deleteTokenByUserId(Long userId);
}
