package com.gaming.project.gamingproject.services.contracts;

/**
 * Service for authentication
 */

public interface AuthoritiesService {

  boolean doesAuthorityExist(String username);

  void deleteAuthority(String username);
}
