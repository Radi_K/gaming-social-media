package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.entities.PostAccessibility;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.CommentVisualizationModel;
import com.gaming.project.gamingproject.models.LikeCommentModel;
import com.gaming.project.gamingproject.models.PostEditModel;
import com.gaming.project.gamingproject.models.PostModel;
import com.gaming.project.gamingproject.models.PostVisualisationModel;
import com.gaming.project.gamingproject.repositories.PostRepository;
import com.gaming.project.gamingproject.services.contracts.FriendService;
import com.gaming.project.gamingproject.services.contracts.PostService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

  private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

  private final PostRepository postRepository;
  private final UserService userService;
  private final ModelMapper mapper;
  private final FriendService friendService;

  @Override
  public Post createPost(Post post) {
    return postRepository.save(post);
  }

  @Override
  public Post getById(Long id) {
    return postRepository
            .findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Post does not "
                    + "exist."));
  }

  @Transactional
  @Override
  public void deletePost(Long postId) {
    if (!postRepository.existsById(postId)) {
      throw new EntityNotFoundException("Post not Found");
    }
    Post post = getById(postId);
    postRepository.delete(post);
    log.warn("Deleted post with id - {}", post.getId());
  }

  @Override
  public PostEditModel editPost(PostEditModel postModel) {
    Post post = getById(postModel.getId());
    post.setText(postModel.getText());
    post.setTotalLikes(postModel.getTotalLikes());
    log.warn("Edited post with id - {}", post.getId());
    return convertPostEditModel(postRepository.save(post));
  }

  @Override
  public Page<PostVisualisationModel> getPostForUserPage(Pageable pageable,
                                                         Principal principal) {
    User user = userService.getUser(principal.getName());
    return postRepository
            .findByUserOrderByUpdateTimeDesc(pageable, user)
            .map(el -> convertPost(el, user));
  }

  @Override
  public Page<PostVisualisationModel> findTop5PostsByLikes(Pageable pageable,
                                                           Principal principal) {
    User loggedUser = userService.getUser(principal.getName());
    return postRepository
            .findAllTopFiveForUser(pageable, loggedUser.getId())
            .map(el -> convertPost(el, loggedUser));
  }

  @Override
  public Page<PostVisualisationModel> findAllPublicPosts(Pageable pageable) {
    return postRepository.findAllPublicPosts(pageable).map(this::convertPost);
  }

  @Override
  public Page<PostVisualisationModel> findPostsForUser(Pageable pageable,
                                                       Long userId) {
    User user = userService.findById(userId);
    return postRepository.findAllByUser(pageable, user).map(this::convertPost);
  }

  @Override
  public Page<PostVisualisationModel> findPostsByUsername(
          Pageable pageable, String userLoginUsername, Principal principal) {
    User user = userService.getUser(userLoginUsername);
    User loggedUser = userService.getUser(principal.getName());
    Page<PostVisualisationModel> posts = null;
    if (friendService.doesFriendshipExist(user, loggedUser)
            || friendService.doesFriendshipExist(loggedUser, user)) {
      posts =
              postRepository
                      .findAllByUserLoginUsernameOrderByIdDesc(userLoginUsername, pageable)
                      .map(el -> convertPost(el, loggedUser));
    } else {
      posts =
              postRepository
                      .findByUserLoginUsernameAndPostAccessibilityEqualsOrderByIdDesc(
                              userLoginUsername, pageable,
                              PostAccessibility.PUBLIC)
                      .map(el -> convertPost(el, loggedUser));
    }

    if (posts.isEmpty()) {
      throw new EntityNotFoundException("No existing posts");
    }
    return posts;
  }

  @Override
  public Page<PostVisualisationModel> postsPagination(Pageable pageable,
                                                      Principal principal) {
    User user = userService.getUser(principal.getName());
    return postRepository.findAllByUserId(pageable, user.getId()).map(el -> convertPost(el, user));
  }

  @Transactional
  @Override
  public PostVisualisationModel addPost(PostModel postModel,
                                        Principal principal) {
    User user = userService.getUser(principal.getName());
    Post post =
            Post.builder()
                    .text(postModel.getText())
                    .user(user)
                    .postAccessibility(PostAccessibility.valueOf(postModel.getPostAccessibility()))
                    .creationTime(LocalDateTime.now())
                    .updateTime(LocalDateTime.now())
                    .videoId(postModel.getVideoId())
                    .photo(postModel.getPhoto())
                    .build();

    user.getPosts().add(post);
    return convertPost(createPost(post));
  }

  private PostEditModel convertPostEditModel(Post post) {
    return mapper.map(post, PostEditModel.class);
  }

  private PostVisualisationModel convertPost(Post post) {
    PostVisualisationModel postVisualisationModel = mapper.map(post, PostVisualisationModel.class);
    postVisualisationModel.setCreationTime(LocalDateTime.parse(post.getCreationTime(), FORMATTER));
    return postVisualisationModel;
  }

  private PostVisualisationModel convertPost(Post post, User user) {
    PostVisualisationModel postVisualisationModel = mapper.map(post, PostVisualisationModel.class);
    checkPostIfLiked(post, user, postVisualisationModel);
    postVisualisationModel.setCreationTime(LocalDateTime.parse(post.getCreationTime(), FORMATTER));

    final List<CommentVisualizationModel> postComments = postVisualisationModel.getComments();
    for (int i = 0; i < postComments.size(); i++) {
      CommentVisualizationModel currentComment = postComments.get(i);
      currentComment.setCreationTime(LocalDateTime.parse(post.getComments().get(i).getCreationTime(), FORMATTER));
      for (LikeCommentModel commentLike : currentComment.getLikes()) {
        if (commentLike.getUserId() == user.getId()) {
          currentComment.setLiked(true);
        }
      }
    }

    return postVisualisationModel;
  }

  private void checkPostIfLiked(Post post, User user, PostVisualisationModel postVisualisationModel) {
    post.getLikes().stream()
            .filter(like -> like.getUser().getId() == user.getId())
            .map(like -> true)
            .forEach(postVisualisationModel::setLiked);
  }

}
