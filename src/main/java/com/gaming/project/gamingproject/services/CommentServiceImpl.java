package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.Comment;
import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.exceptions.EntityNotFoundException;
import com.gaming.project.gamingproject.models.CommentEditModel;
import com.gaming.project.gamingproject.models.CommentModel;
import com.gaming.project.gamingproject.models.CommentVisualizationModel;
import com.gaming.project.gamingproject.repositories.CommentRepository;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import com.gaming.project.gamingproject.services.contracts.PostService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

  private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

  private final CommentRepository commentRepository;
  private final PostService postService;
  private final UserService userService;
  private final ModelMapper mapper;

  @Override
  public Page<CommentVisualizationModel> findCommentsForUser(Pageable pageable, Long userId) {
    return commentRepository.findAllByUserId(pageable, userId).map(this::convertComment);
  }

  @Override
  public void deleteCommentById(Long commentId) {
    commentRepository.delete(getById(commentId));
    log.warn("Deleted comment with id - {}", commentId);
  }

  @Override
  public CommentEditModel editComment(CommentEditModel commentModel) {
    Comment comment = getById(commentModel.getId());
    comment.setText(commentModel.getText());
    comment.setTotalLikes(commentModel.getTotalLikes());
    log.warn("Edited comment with id - {}", comment.getId());
    return convertCommentEditModel(commentRepository.save(comment));
  }

  @Override
  public Comment createComment(Comment comment) {
    return commentRepository.save(comment);
  }

  @Override
  public CommentVisualizationModel convertComment(Comment comment) {
    CommentVisualizationModel model = mapper.map(comment, CommentVisualizationModel.class);
    model.setCreationTime(LocalDateTime.parse(comment.getCreationTime(), FORMATTER));
    return model;
  }

  @Override
  public Comment getById(Long id) {
    return commentRepository
            .findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Comment does not "
                    + "exist."));
  }

  @Override
  public CommentVisualizationModel addCommentToPost(
          CommentModel commentModel, Principal principal) {
    User user = userService.getUser(principal.getName());
    Post post = postService.getById(commentModel.getPostId());
    post.setUpdateTime(LocalDateTime.now());

    Comment comment = new Comment(commentModel.getText(), 0, post, user,
            LocalDateTime.now());

    return convertComment(createComment(comment));
  }

  private CommentEditModel convertCommentEditModel(Comment comment) {
    return mapper.map(comment, CommentEditModel.class);
  }

}
