package com.gaming.project.gamingproject.services;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.services.contracts.ConfirmationTokenService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class EmailSenderService {

  private static final String RESET_PASSWORD_LINK_MESSAGE = "To complete the password reset process," +
          " please click here: http://localhost:8080/reset/confirmation?token=";

  private final JavaMailSender javaMailSender;
  private final UserService userService;
  private final ConfirmationTokenService tokenService;

  @Async
  public void sendMailForRegister(RegisterUserModel user, String mailUsername, HttpServletRequest request) {
    ConfirmationToken token = userService.createUserAndToken(user);
    SimpleMailMessage mailMessage = new SimpleMailMessage();
    registrationMailMessage(user, mailUsername, request, token, mailMessage);
    javaMailSender.send(mailMessage);
  }

  @Async
  public void sendMailForPasswordReset(User existingUser, String mailUsername) {
    ConfirmationToken confirmationToken = new ConfirmationToken(existingUser);
    tokenService.createToken(confirmationToken);
    SimpleMailMessage mailMessage = new SimpleMailMessage();
    passwordRestorationMessage(existingUser, confirmationToken, mailMessage, mailUsername);
    javaMailSender.send(mailMessage);
  }

  private void passwordRestorationMessage(User existingUser, ConfirmationToken confirmationToken,
                                          SimpleMailMessage mailMessage, String mailUsername) {
    mailMessage.setTo(existingUser.getEmail());
    mailMessage.setSubject("Complete Password Reset!");
    mailMessage.setFrom(mailUsername);
    mailMessage.setText(RESET_PASSWORD_LINK_MESSAGE + confirmationToken.getConfirmationToken());
  }

  private void registrationMailMessage(RegisterUserModel user, String mailUsername, HttpServletRequest request,
                                       ConfirmationToken token, SimpleMailMessage mailMessage) {
    mailMessage.setTo(user.getEmail());
    mailMessage.setSubject("Complete Registration!");
    mailMessage.setFrom(mailUsername);
    mailMessage.setText(
            "To confirm your account, please click here : "
                    + request.getRequestURL().toString()
                    + "account/verified?token="
                    + token.getConfirmationToken());
  }
}
