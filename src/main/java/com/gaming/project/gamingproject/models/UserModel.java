package com.gaming.project.gamingproject.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserModel {

  @PositiveOrZero
  private Long id;

  @Size(min = 2, max = 24)
  @NotBlank
  private String firstName;

  @Size(min = 2, max = 24)
  @NotBlank
  private String lastName;

  @Size(min = 5, max = 34)
  @NotBlank
  private String email;

  @PositiveOrZero
  private Integer age;

  @NotNull
  @NotBlank
  private String gender;

  @NotNull
  @NotBlank
  private String country;

  @Size(min = 5, max = 100)
  private String description;

  private String loginUsername;

  private String picture;

  private boolean hasFriend;

  private List<PostVisualisationModel> posts;

  private List<CommentVisualizationModel> comments;
}
