package com.gaming.project.gamingproject.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FriendVisualisationModel {

  private String senderFirstName;
  private String senderLastName;
  private String senderLoginUserName;
  private String senderPicture;
  private String receiverPicture;
}
