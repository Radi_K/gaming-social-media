package com.gaming.project.gamingproject.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostVisualisationModel {

  private Long id;
  private String text;
  private String userFirstName;
  private String userLastName;
  private Integer totalLikes;
  private List<CommentVisualizationModel> comments;
  private String userPicture;
  private boolean isLiked;
  private LocalDateTime creationTime;
  private String contentPicture;
  private String userLoginUsername;
  private String videoId;
  private String photo;
}
