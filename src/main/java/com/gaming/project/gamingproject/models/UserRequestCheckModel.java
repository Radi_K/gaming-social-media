package com.gaming.project.gamingproject.models;

import com.gaming.project.gamingproject.entities.RequestType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestCheckModel {

  private RequestType requestType;
}
