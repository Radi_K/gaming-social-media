package com.gaming.project.gamingproject.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentVisualizationModel {

  @PositiveOrZero
  private Long id;

  @Size(min = 1, max = 255)
  @NotNull
  private String text;

  @NotNull
  private String userFirstName;

  @NotNull
  private String userLastName;

  private String userPicture;

  private boolean isLiked;

  private LocalDateTime creationTime;

  private List<LikeCommentModel> likes;

  @PositiveOrZero
  private Integer totalLikes;

  private String userLoginUsername;
}
