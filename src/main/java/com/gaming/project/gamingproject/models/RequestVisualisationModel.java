package com.gaming.project.gamingproject.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestVisualisationModel {

  private Long id;
  private Long senderId;
  private Long receiverId;
  private String senderFirstName;
  private String senderLastName;
  private String senderPicture;
  private String senderLoginUsername;
}
