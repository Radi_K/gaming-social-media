package com.gaming.project.gamingproject.entities;

public enum PostAccessibility {
  PUBLIC,
  PRIVATE
}
