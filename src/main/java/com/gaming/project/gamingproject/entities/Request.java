package com.gaming.project.gamingproject.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "friend_request")
public class Request {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(referencedColumnName = "id")
  private User receiver;

  @ManyToOne
  @JoinColumn(referencedColumnName = "id")
  private User sender;

  public Request(User receiver, User sender) {
    this.receiver = receiver;
    this.sender = sender;
  }
}
