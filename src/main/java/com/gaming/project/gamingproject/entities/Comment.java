package com.gaming.project.gamingproject.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "comments")
public class Comment {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "content")
  @NotBlank
  private String text;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "comment_id")
  private List<LikeComment> likes;

  @Column(name = "likes")
  @PositiveOrZero
  private int totalLikes;

  @OneToOne
  @JoinColumn(name = "post_id")
  @NotNull
  private Post post;

  @OneToMany
  @JoinColumn(name = "comment_id")
  private List<CommentReply> replies;

  @OneToOne
  @JoinColumn(name = "user_id")
  private User user;

  @Column(name = "creation_time")
  @Convert(converter = DataTimeConverter.class)
  private LocalDateTime creationTime;

  public Comment(
          String text, int totalLikes, @NotNull Post post, User user,
          LocalDateTime creationTime) {
    this.text = text;
    this.totalLikes = totalLikes;
    this.post = post;
    this.user = user;
    this.creationTime = creationTime;
  }

  public String getCreationTime() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy "
            + "HH:mm");
    return creationTime.format(formatter);
  }
}
