package com.gaming.project.gamingproject.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "posts")
public class Post {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "text")
  @NotNull
  private String text;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "post_id")
  private List<Like> likes;

  @Column(name = "likes")
  private int totalLikes;

  @OneToOne
  @JoinColumn(name = "user_id")
  private User user;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "post_id")
  private List<Comment> comments;

  @Enumerated(EnumType.STRING)
  private PostAccessibility postAccessibility;

  @Column(name = "creation_time")
  @Convert(converter = DataTimeConverter.class)
  private LocalDateTime creationTime;

  @Column(name = "updated_time")
  @Convert(converter = DataTimeConverter.class)
  private LocalDateTime updateTime;

  @Column(name = "video_id")
  private String videoId;

  @Column(name = "photo")
  private String photo;

  public String getCreationTime() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy "
            + "HH:mm");
    return creationTime.format(formatter);
  }
}
