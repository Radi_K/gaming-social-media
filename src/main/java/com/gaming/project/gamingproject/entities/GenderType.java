package com.gaming.project.gamingproject.entities;

public enum GenderType {
  MALE,
  FEMALE,
  OTHER;
}
