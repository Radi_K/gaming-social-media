package com.gaming.project.gamingproject.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "users_no")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "first_name")
  @Size(min = 2, max = 20)
  private String firstName;

  @Column(name = "last_name")
  @Size(min = 2, max = 20)
  private String lastName;

  @Column(name = "email")
  @Size(min = 4, max = 40)
  private String email;

  @Column(name = "age")
  private int age;

  @Enumerated(EnumType.STRING)
  @NotNull
  private GenderType gender;

  @Column(name = "country")
  @Size(min = 4, max = 30)
  private String country;

  @Column(name = "description")
  @Size(min = 5, max = 100)
  private String description;

  @OneToOne(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "login_id")
  @NotNull
  private Login login;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "user_id")
  private List<Post> posts;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "user_id")
  private List<Comment> comments;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "user_id")
  private List<Like> likes;

  @Column(name = "picture")
  private String picture;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "user_id")
  private List<CommentReply> commentReplies;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "sender_id")
  private List<Request> sentFriendRequest;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "receiver_id")
  private List<Request> receivedFriendRequest;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "sender_id")
  private List<Friend> senderFriendList;

  @OneToMany(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "receiver_id")
  private List<Friend> receiverFriendList;

  @Column(name = "register_time")
  @Convert(converter = DataTimeConverter.class)
  private LocalDateTime registerTime;
}
