package com.gaming.project.gamingproject.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "likes_comment")
public class LikeComment {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @OneToOne
  @JoinColumn(name = "comment_id")
  private Comment comment;

  @OneToOne
  @JoinColumn(name = "user_id")
  private User user;

  public LikeComment(Comment comment, User user) {
    this.comment = comment;
    this.user = user;
  }
}
