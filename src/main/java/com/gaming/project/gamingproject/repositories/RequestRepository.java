package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Request;
import com.gaming.project.gamingproject.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository with functions for friend requests
 */

@Repository
public interface RequestRepository extends CrudRepository<Request, Long> {

  boolean existsBySenderIdAndReceiverId(Long senderId, Long receiverId);

  void deleteBySenderAndReceiver(User sender, User receiver);

  Page<Request> findAllByReceiverLoginUsername(String receiverLoginUsername,
                                               Pageable pageable);
}
