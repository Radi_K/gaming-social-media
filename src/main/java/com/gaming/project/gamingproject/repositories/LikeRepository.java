package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Like;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Operations for checking and getting likes
 */

@Repository
public interface LikeRepository extends CrudRepository<Like, Long> {

  boolean existsByPostIdAndUserId(Long postId, Long userId);

  Like findByUserIdAndPostId(Long userId, Long postId);
}
