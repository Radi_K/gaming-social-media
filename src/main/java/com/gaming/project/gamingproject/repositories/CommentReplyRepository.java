package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.CommentReply;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * This is repository for taking comment replies from database
 */

@Repository
public interface CommentReplyRepository extends CrudRepository<CommentReply,
        Long> {
}
