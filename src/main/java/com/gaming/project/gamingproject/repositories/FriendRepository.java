package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Friend;
import com.gaming.project.gamingproject.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interface which is for friendship between user1 + user2
 */

@Repository
public interface FriendRepository extends CrudRepository<Friend, Long> {

  boolean existsBySenderIdAndReceiverId(Long senderId, Long receiverId);

  void deleteBySenderAndReceiver(User sender, User receiver);

  List<Friend> findAllByReceiverLoginUsername(String userLoginUsername);
}
