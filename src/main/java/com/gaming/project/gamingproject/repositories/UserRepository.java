package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository with operations for users
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  boolean existsById(Long userId);

  List<User> findByGenderEquals(GenderType gender);

  User findFirstByLoginUsername(String userUsername);

  List<User> findByFirstNameStartingWithOrLastNameStartingWith(String firstName, String lastName);

  @Query("select u from User u where u.login.username not like ?1")
  Page<User> findAll(Pageable pageable, String username);

  User findByEmailIgnoreCase(String email);

  boolean existsByLoginUsername(String userLoginName);
}
