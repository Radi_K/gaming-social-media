package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Post;
import com.gaming.project.gamingproject.entities.PostAccessibility;
import com.gaming.project.gamingproject.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository with many operations which is used
 * for extracting posts from database table
 */

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

  Page<Post> findAllByUser(Pageable pageable, User user);

  Page<Post> findAllByUserLoginUsernameOrderByIdDesc(String userLoginUsername,
                                                     Pageable pageable);

  Page<Post> findByUserLoginUsernameAndPostAccessibilityEqualsOrderByIdDesc(
          String userLoginUsername, Pageable pageable,
          PostAccessibility postAccessibility);

  Page<Post> findByUserOrderByUpdateTimeDesc(Pageable pageable, User user);

  @Query(
          "select p from Post p "
                  + "where (p.user.id in (select f.sender.id from Friend f "
                  + "where f.receiver.id = ?1) "
                  + "or p.user.id in (select f.receiver.id from Friend f "
                  + "where f.sender.id = ?1))"
                  + "or p.postAccessibility = 'PUBLIC' "
                  + "order by p.updateTime desc")
  Page<Post> findAllByUserId(Pageable pageable, Long userId);

  @Query(
          "select p from Post p "
                  + "where (p.user.id in (select f.sender.id from Friend f "
                  + "where f.receiver.id = ?1) "
                  + "or p.user.id in (select f.receiver.id from Friend f "
                  + "where f.sender.id = ?1))"
                  + "or p.postAccessibility = 'PUBLIC'"
                  + "order by p.totalLikes desc")
  Page<Post> findAllTopFiveForUser(Pageable pageable, Long userId);

  @Query(
          "select p from Post p "
                  + "where p.postAccessibility = 'PUBLIC' "
                  + "order by p.updateTime desc")
  Page<Post> findAllPublicPosts(Pageable pageable);
}
