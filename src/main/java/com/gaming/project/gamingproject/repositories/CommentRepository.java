package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This is repository for taking comments from database and
 * do some operations
 */

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

  void deleteAllByPostId(Long postId);

  Page<Comment> findAllByUserId(Pageable pageable, Long userId);
}
