package com.gaming.project.gamingproject.repositories;

import com.gaming.project.gamingproject.entities.LikeComment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 */

@Repository
public interface LikeCommentRepository extends CrudRepository<LikeComment,
        Long> {

  boolean existsByCommentIdAndUserId(Long commentId, Long userId);

  LikeComment findByUserIdAndCommentId(Long userId, Long commentId);

  void deleteAllByUserId(Long userId);

  void deleteAllByCommentId(Long commentId);
}
