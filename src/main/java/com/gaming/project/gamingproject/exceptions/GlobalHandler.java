package com.gaming.project.gamingproject.exceptions;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class GlobalHandler extends ResponseEntityExceptionHandler {

  private final ModelAndView model;

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler
  public ModelAndView handleEntityNotFound(EntityNotFoundException exception) {
    log.error("Handle EntityNotFound - {}", exception.getMessage());
    model.addObject("errorMessage", exception.getMessage());
    model.addObject("statusCode", HttpStatus.NOT_FOUND);
    model.setViewName("error");
    return model;
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler
  public ModelAndView handleEntityAlreadyExist(EntityAlreadyExist exception) {
    log.error("Handle EntityAlreadyExist - {}", exception.getMessage());
    model.addObject("errorMessage", exception.getMessage());
    model.addObject("statusCode", HttpStatus.BAD_REQUEST);
    model.setViewName("error");
    return model;
  }

  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(Exception.class)
  public ModelAndView handleAnyException() {
    log.error("Handle unexpected error");
    model.addObject("errorMessage", "Unexpected error occurred!");
    model.setViewName("error");
    return model;
  }
}
