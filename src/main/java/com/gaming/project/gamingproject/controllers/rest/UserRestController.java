package com.gaming.project.gamingproject.controllers.rest;

import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.models.CommentModel;
import com.gaming.project.gamingproject.models.CommentVisualizationModel;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.models.UserModel;
import com.gaming.project.gamingproject.models.UserRequestCheckModel;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import com.gaming.project.gamingproject.services.contracts.FriendService;
import com.gaming.project.gamingproject.services.contracts.PostService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Api(tags = {"User Rest Controller"})
public class UserRestController {

  private final UserService userService;
  private final CommentService commentService;
  private final FriendService friendService;

  @ApiOperation(value = "Retrieve UserModel by id", response = UserModel.class)
  @ApiResponses(
          value = {
                  @ApiResponse(code = 200, message = "Successfully retrieved "
                          + "user"),
                  @ApiResponse(code = 404, message = "The resource you were "
                          + "trying to reach is not found")
          })
  @GetMapping("/{userId}")
  public UserModel getUser(
      /*@ApiParam(value = "Integer value by which user with current id will
      be retrieved",
      required = true)*/ @PositiveOrZero @NotNull @PathVariable Long userId) {
    return userService.findByIdModel(userId);
  }

  @GetMapping("/check/{userToCheckId}")
  public UserRequestCheckModel getUserButtonCheck(
      /*@ApiParam(value = "Integer value by which user with current id will
      be retrieved",
      required = true)*/ @PositiveOrZero @NotNull @PathVariable Long userToCheckId,
                         Principal principal) {
    return friendService.checkUserFriendship(userToCheckId, principal);
  }

  @GetMapping("/logged")
  public UserModel getLoggedUser(Principal principal) {
    User loggedUser = userService.getUser(principal.getName());

    return userService.findByIdModel(loggedUser.getId());
  }

  @ApiOperation(value = "Get List of UserModels filtered by first name",
          response = List.class)
  @GetMapping("/filter")
  public List<UserModel> getAllUsersByFirstNameOrLastName(
          @ApiParam(
                  value =
                          "String value which produces list wish user"
                                  + " models if value is equal as first name "
                                  + "of some user",
                  required = true)
          @RequestParam
                  String firstName,
          @RequestParam String lastName) {
    return userService.getAllUsersByFirstNameOrLastName(firstName, lastName);
  }

  @ApiOperation(value = "Get Page of user models", response = Page.class)
  @GetMapping("/page")
  public Page<UserModel> getPage(Pageable pageable, Principal principal) {
    return userService.userPagination(pageable, principal);
  }

  @ApiOperation(value = "Add new user")
  @PostMapping("/")
  public void addUser(
          @ApiParam(value = "User object store in database table", required =
                  true) @RequestBody @Valid
                  RegisterUserModel user) {
    userService.createUser(user);
  }

  @ApiOperation(value = "Add comment to post", response =
          CommentVisualizationModel.class)
  @PostMapping("/comments")
  public CommentVisualizationModel addCommentToPost(
          @ApiParam(value = "Comment model which will be saved in database",
                  required = true)
          @RequestBody
          @Valid
                  CommentModel commentModel,
          Principal principal) {
    return commentService.addCommentToPost(commentModel, principal);
  }

  @ApiOperation(value = "Edit fields of the chosen user", response =
          UserModel.class)
  @PutMapping("/")
  public UserModel editUser(
          @ApiParam(
                  value =
                          "Passed edit user as model which will replace "
                                  + "already existing"
                                  + " user in database",
                  required = true)
          @RequestBody
          @Valid
                  UserModel userModel) {
    return userService.editUser(userModel);
  }

  @ApiOperation(value = "Edit fields of the chosen user", response =
          UserModel.class)
  @DeleteMapping("/{userId}")
  public void deleteUser(@PositiveOrZero @NotNull @PathVariable Long userId) {
    userService.deleteUserById(userId);
  }
}
