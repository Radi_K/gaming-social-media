package com.gaming.project.gamingproject.controllers;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.services.EmailSenderService;
import com.gaming.project.gamingproject.services.contracts.ConfirmationTokenService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class ForgotPasswordController {

  private static final String PASSWORD_REQUEST_MESSAGE = "Request to reset password received. " +
          "Check your inbox for the reset link.";
  private static final String PASSWORD_SUCCESSFUL_RESET_MESSAGE = "Password successfully reset. You can now log in "
          + "with the new credentials.";
  private static final String INVALID_LINK_MESSAGE = "The link is invalid or broken!";

  private final UserService userService;
  private final ConfirmationTokenService tokenService;
  private final EmailSenderService emailSenderService;

  @Value("spring.mail.username")
  private String mailUsername;

  @GetMapping("/password/reset")
  public ModelAndView displayResetPassword(ModelAndView modelAndView,
                                           User user) {
    modelAndView.addObject("user", user);
    modelAndView.setViewName("forgot-password");
    return modelAndView;
  }

  @PostMapping("/password/reset")
  public ModelAndView forgotUserPassword(ModelAndView modelAndView, User user) {
    User existingUser = userService.findByEmailIgnoreCase(user.getEmail());
    if (existingUser != null) {
      emailSenderService.sendMailForPasswordReset(existingUser, mailUsername);
      modelAndView.addObject("message", PASSWORD_REQUEST_MESSAGE);
      modelAndView.setViewName("success-forgot-password");
    } else {
      modelAndView.addObject("message", INVALID_LINK_MESSAGE);
      modelAndView.setViewName("error");
    }
    return modelAndView;
  }

  @GetMapping("/reset/confirmation")
  public ModelAndView validateResetToken(
          ModelAndView modelAndView,
          @RequestParam("token") String confirmationToken) {
    ConfirmationToken token =
            tokenService.findByConfirmationToken(confirmationToken);
    if (token != null) {
      User user = userService.retrieveUser(token);
      modelAndView.addObject("user", user);
      modelAndView.addObject("email", user.getEmail());
      modelAndView.setViewName("confirm-reset");
    } else {
      modelAndView.addObject("message", INVALID_LINK_MESSAGE);
      modelAndView.setViewName("error");
    }
    return modelAndView;
  }

  @PostMapping("/password/retrieval")
  public ModelAndView resetUserPassword(ModelAndView modelAndView, User user) {
    if (user.getEmail() != null) {
      userService.userNewPassword(user);
      modelAndView.addObject("message", PASSWORD_SUCCESSFUL_RESET_MESSAGE);
      modelAndView.setViewName("success-reset-password");
    } else {
      modelAndView.addObject("message", INVALID_LINK_MESSAGE);
      modelAndView.setViewName("error");
    }
    return modelAndView;
  }
}
