package com.gaming.project.gamingproject.controllers.rest;

import com.gaming.project.gamingproject.models.FriendVisualisationModel;
import com.gaming.project.gamingproject.models.UserModel;
import com.gaming.project.gamingproject.services.contracts.FriendService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/friends")
@RequiredArgsConstructor
public class FriendRestController {

  private final FriendService friendService;

  @GetMapping("/{username}")
  public List<FriendVisualisationModel> getFriends(
          @Size(min = 2, max = 24) @NotNull @PathVariable String username) {
    return friendService.convertListFriend(friendService.findFriendsByUserName(username));
  }

  @PostMapping("/{wantedUserId}")
  public void addInFriendList(
          @PositiveOrZero @NotNull @PathVariable Long wantedUserId,
          Principal principal) {
    friendService.addAsFriend(wantedUserId, principal);
  }

  @DeleteMapping("/{wantedUserId}")
  public void removeFriend(
          @PositiveOrZero @NotNull @PathVariable Long wantedUserId,
          Principal principal) {
    friendService.removeFromFriends(wantedUserId, principal);
  }

  @PutMapping("/check")
  public List<UserModel> checkFriendShipOrRequest(@RequestBody List<UserModel> usersToCheck, Principal principal) {
    return friendService.checkUsersForFriendshipOrRequest(usersToCheck, principal);
  }
}
