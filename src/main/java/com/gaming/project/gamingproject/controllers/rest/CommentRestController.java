package com.gaming.project.gamingproject.controllers.rest;

import com.gaming.project.gamingproject.models.CommentEditModel;
import com.gaming.project.gamingproject.models.CommentVisualizationModel;
import com.gaming.project.gamingproject.models.LikeCommentModel;
import com.gaming.project.gamingproject.services.contracts.CommentService;
import com.gaming.project.gamingproject.services.contracts.LikeCommentService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.security.Principal;

@RestController
@RequestMapping("/comments")
@RequiredArgsConstructor
public class CommentRestController {

  private final LikeCommentService likeCommentService;
  private final CommentService commentService;

  @ApiOperation(value = "Get Page of comment models for user with id/",
          response = Page.class)
  @GetMapping("/page/{userId}")
  public Page<CommentVisualizationModel> getPostsByPageForUser(
          Pageable pageable,
          @PositiveOrZero @NotNull @PathVariable Long userId) {
    return commentService.findCommentsForUser(pageable, userId);
  }

  @PostMapping("/like")
  public Integer addLikeToPost(
          @RequestBody LikeCommentModel likeCommentModel, Principal principal) {
    return likeCommentService.addLikeToComment(likeCommentModel, principal);
  }

  @DeleteMapping("/dislike")
  public Integer dislikePost(@RequestBody LikeCommentModel likeCommentModel,
                             Principal principal) {
    return likeCommentService.dislike(likeCommentModel, principal);
  }

  @ApiOperation(value = "Edit comment by given id and model which have to "
          + "contains some values")
  @PutMapping("/")
  public CommentEditModel editPostById(@RequestBody CommentEditModel commentModel) {
    return commentService.editComment(commentModel);
  }

  @DeleteMapping("/{commentId}")
  public void deleteCommentById(@PositiveOrZero @NotNull @PathVariable Long commentId) {
    commentService.deleteCommentById(commentId);
  }
}
