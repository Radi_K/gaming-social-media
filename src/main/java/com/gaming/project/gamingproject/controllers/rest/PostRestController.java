package com.gaming.project.gamingproject.controllers.rest;

import com.gaming.project.gamingproject.models.LikeModel;
import com.gaming.project.gamingproject.models.PostEditModel;
import com.gaming.project.gamingproject.models.PostModel;
import com.gaming.project.gamingproject.models.PostVisualisationModel;
import com.gaming.project.gamingproject.services.contracts.LikeService;
import com.gaming.project.gamingproject.services.contracts.PostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.security.Principal;

@RestController
@RequestMapping("/posts")
@RequiredArgsConstructor
@Api(tags = {"Post Rest Controller"})
public class PostRestController {

  private final PostService postService;
  private final LikeService likeService;

  @ApiOperation(value = "Get Page of post models for logged user", response =
          Page.class)
  @GetMapping("/page")
  public Page<PostVisualisationModel> getPostsByPage(Pageable pageable,
                                                     Principal principal) {
    return postService.postsPagination(pageable, principal);
  }

  @ApiOperation(value = "Get Page of post models for user with id/username",
          response = Page.class)
  @GetMapping("/page/{userId}")
  public Page<PostVisualisationModel> getPostsByPageForUser(
          Pageable pageable,
          @PositiveOrZero @NotNull @PathVariable Long userId) {
    return postService.findPostsForUser(pageable, userId);
  }

  @ApiOperation(value = "Get list of post models for logged user", response =
          Page.class)
  @GetMapping("/")
  public Page<PostVisualisationModel> getUserPosts(Pageable pageable,
                                                   Principal principal) {
    return postService.getPostForUserPage(pageable, principal);
  }

  @GetMapping("/page/top")
  public Page<PostVisualisationModel> topFiveByLikes(Pageable pageable,
                                                     Principal principal) {
    return postService.findTop5PostsByLikes(pageable, principal);
  }

  @ApiOperation(value = "Get list of post models for logged user", response =
          Page.class)
  @GetMapping("/{username}")
  public Page<PostVisualisationModel> getOtherUserPosts(
          @ApiParam(
                  value = "String username value which produces list with "
                          + "post" + " models",
                  required = true)
                  Pageable pageable,
          @Size(min = 2, max = 24) @PathVariable String username,
          Principal principal) {
    return postService.findPostsByUsername(pageable, username, principal);
  }

  @ApiOperation(value = "Add post")
  @PostMapping("/")
  public PostVisualisationModel addPost(
          @ApiParam(value = "Post model which will be saved in database",
                  required = true)
          @RequestBody
          @Valid
                  PostModel postModel,
          Principal principal) {
    return postService.addPost(postModel, principal);
  }

  @ApiOperation(value = "Add like to post")
  @PostMapping("/like")
  public Integer addLikeToPost(@RequestBody @Valid LikeModel likeModel,
                               Principal principal) {
    return likeService.addLikeToPost(likeModel, principal);
  }

  @ApiOperation(value = "Remove like from post - dislike")
  @DeleteMapping("/dislike")
  public Integer dislikePost(@RequestBody @Valid LikeModel likeModel,
                             Principal principal) {
    return likeService.dislike(likeModel, principal);
  }

  @ApiOperation(value = "Edit post by given id and model which have to "
          + "contains some values")
  @PutMapping("/")
  public PostEditModel editPostById(@RequestBody @Valid PostEditModel postModel) {
    return postService.editPost(postModel);
  }

  @ApiOperation(value = "Delete post with chosen id")
  @DeleteMapping("/{postId}")
  public void deletePost(@PositiveOrZero @NotNull @PathVariable Long postId) {
    postService.deletePost(postId);
  }

  @GetMapping("/page/public")
  public Page<PostVisualisationModel> getAllPublicPosts(Pageable pageable) {
    return postService.findAllPublicPosts(pageable);
  }
}
