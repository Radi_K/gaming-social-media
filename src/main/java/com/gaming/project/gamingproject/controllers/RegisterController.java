package com.gaming.project.gamingproject.controllers;

import com.gaming.project.gamingproject.entities.ConfirmationToken;
import com.gaming.project.gamingproject.entities.GenderType;
import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.entities.UserCountries;
import com.gaming.project.gamingproject.models.LoginModel;
import com.gaming.project.gamingproject.models.RegisterUserModel;
import com.gaming.project.gamingproject.services.EmailSenderService;
import com.gaming.project.gamingproject.services.contracts.ConfirmationTokenService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class RegisterController {

  private static final String INVALID_LINK_MESSAGE = "The link is invalid or broken!";
  private static final String EMAIL_ALREADY_EXIST_MESSAGE = "This email already exists!";

  private final UserService userService;
  private final EmailSenderService emailSenderService;
  private final ConfirmationTokenService tokenService;

  @Value("spring.mail.username")
  private String mailUsername;

  @GetMapping("/")
  public ModelAndView passValuesLoginPage(ModelAndView modelAndView,
                                          RegisterUserModel user) {
    modelAndView.addObject("login", new LoginModel());
    modelAndView.addObject("user", user);
    modelAndView.addObject("countries", UserCountries.values());
    modelAndView.addObject("genders", GenderType.values());
    modelAndView.setViewName("index");
    return modelAndView;
  }

  @PostMapping(value = "/")
  public ModelAndView registerUserEmail(
          ModelAndView modelAndView, @Valid RegisterUserModel user,
          HttpServletRequest request) {
    User existingUser = userService.findByEmailIgnoreCase(user.getEmail());
    if (existingUser != null) {
      modelAndView.addObject("message", EMAIL_ALREADY_EXIST_MESSAGE);
      modelAndView.setViewName("error");
    } else {
      emailSenderService.sendMailForRegister(user, mailUsername, request);
      modelAndView.addObject("emailId", user.getEmail());
      modelAndView.setViewName("register-success");
    }
    return modelAndView;
  }

  @GetMapping("/account/verified")
  public ModelAndView confirmUserAccount(
          ModelAndView modelAndView,
          @RequestParam("token") String confirmationToken) {
    ConfirmationToken token =
            tokenService.findByConfirmationToken(confirmationToken);
    if (token != null) {
      userService.retrieveUser(token);
      modelAndView.setViewName("account-verified");
    } else {
      modelAndView.addObject("message", INVALID_LINK_MESSAGE);
      modelAndView.setViewName("error");
    }
    return modelAndView;
  }

  @GetMapping("/register-confirmation")
  public String registerConfirmation() {
    return "register-confirmation";
  }
}
