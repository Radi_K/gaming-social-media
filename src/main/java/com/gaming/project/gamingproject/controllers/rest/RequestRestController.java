package com.gaming.project.gamingproject.controllers.rest;

import com.gaming.project.gamingproject.models.RequestVisualisationModel;
import com.gaming.project.gamingproject.services.contracts.RequestService;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.security.Principal;

@RestController
@RequestMapping("/request")
@RequiredArgsConstructor
public class RequestRestController {

  private final RequestService requestService;

  @PostMapping("/{userReceivingRequestId}")
  public void addFriend(
          @PositiveOrZero @NotNull @PathVariable Long userReceivingRequestId,
          Principal principal) {
    requestService.sendRequest(userReceivingRequestId, principal);
  }

  @DeleteMapping("/{firstConnectedUserId}-{secondConnectedUserId}")
  public void removeFriendRequest(
          @PositiveOrZero @NotNull @PathVariable Long firstConnectedUserId,
          @PathVariable @PositiveOrZero @NotNull Long secondConnectedUserId) {
    requestService.removeFriendRequest(firstConnectedUserId, secondConnectedUserId);
  }

  @GetMapping("/page")
  public Page<RequestVisualisationModel> getPostsByPage(Principal principal,
                                                        Pageable pageable) {
    return requestService.getAllRequestsByUserName(principal, pageable);
  }
}
