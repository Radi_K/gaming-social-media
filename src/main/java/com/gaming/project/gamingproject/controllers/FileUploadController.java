package com.gaming.project.gamingproject.controllers;

import com.gaming.project.gamingproject.entities.User;
import com.gaming.project.gamingproject.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class FileUploadController {

  public static String uploadDirectory =
          System.getProperty("user.dir") + "/src/main/resources/static/images";
  private final UserService userService;

  @PostMapping("/user/uploadAction")
  public String upload(
          Model model, @RequestParam("files") MultipartFile[] files,
          Principal principal) {
    StringBuilder fileNames = new StringBuilder();
    for (MultipartFile file : files) {
      Path fileNameAndPath = Paths.get(uploadDirectory,
              file.getOriginalFilename());
      fileNames.append(file.getOriginalFilename()).append(" ");
      try {
        Files.write(fileNameAndPath, file.getBytes());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    User user = userService.getUser(principal.getName());

    user.setPicture(fileNames.toString());

    userService.addUser(user);

    return "redirect:/user";
  }

  @PostMapping("/post/uploadPhoto")
  public String uploadPost(@RequestParam("files") MultipartFile[] files) {
    StringBuilder fileNames = new StringBuilder();
    for (MultipartFile file : files) {
      Path fileNameAndPath = Paths.get(uploadDirectory,
              file.getOriginalFilename());
      fileNames.append(file.getOriginalFilename()).append(" ");
      try {
        Files.write(fileNameAndPath, file.getBytes());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    return "redirect:/user-page";
  }
}
