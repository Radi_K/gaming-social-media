let page = 0;
$(document).ready(function () {
    getPosts(3, page);
    addComment();
    getFriends();
    addPost();
    likePost();
    infiniteScroll();
    fillEditPopup();
    likeComment();
    hidePostErrorMessage();
    deletePostWall();
});

let userName = $("#username-field").text();

function infiniteScroll() {
    let working = false;
    $(window).scroll(function () {
        if ($(this).scrollTop() + 1 >= $('body').height() - $(window).height()) {
            if (working === false) {
                working = true;
                page = page + 1;
                getPosts(3, page);
                setTimeout(function () {
                    working = false;
                }, 2000)
            }
        }
    })
}

function getPosts(size, page) {
    $.ajax({
        url: window.location.origin + "/posts/",
        type: "GET",
        data: {
            size: size,
            page: page,
        },
        success: function (response) {
            console.log(response);

            response.content.forEach(function (el) {
                $("#post-conteiner").append(
                    "<div id='post-" + el.id + "' class=\"panel panel-default\">\n" +
                    "                <div class=\"col-md-12\">\n" +
                    "                    <div class=\"media\">\n" +
                    "                        <div class=\"media-left\"><a href=\"/user"+el.userLoginUsername+"\" > <img\n" +
                    "                                src= 'images/" + el.userPicture + "' alt=\"\" class=\"media-object\">\n" +
                    "                        </a></div>\n" +
                    "                        <div class=\"media-body\">\n" +
                    "                            <br>\n" +
                    "<button id=\"delete-post\" type=\"submit\" class=\"btn btn-warning pull-right delete-post-wall\"> Delete</button>" +
                    "                            <h4 class=\"media-heading\"><span>" + el.userFirstName + " " + el.userLastName + " </span> <br>\n" +
                    "                                <small><i class=\"fa fa-clock-o\"></i>  " + el.creationTime + "</small></h4>\n" +
                    "                            <p><span>" + el.text + "</span></p>\n" +
                    "<div class='photo-section-add'></div>" +
                    "\n" +
                    "<div class='post-video-test'></div>\n" +
                    "                            <ul class=\"nav nav-pills pull-left haha\">\n" +
                    "                                <li>" +
                    "<a href=\"\" class='like-button' style='cursor: default; pointer-events: none'>" +
                    "<i class=\"glyphicon glyphicon-thumbs-up\"></i> " +
                    "<span class='total-likes-value'> " + el.totalLikes + " </span> </a></li>\n" +
                    "                                <li>" +
                    "<a href=\"\" class='comment-button' style='cursor: default; pointer-events: none'>" +
                    "<i class=\" glyphicon glyphicon-comment\" ></i> " +
                    "<span class='total-comments-value'>" + el.comments.length + "  </span></a></li>\n" +
                    "                            </ul>\n" +
                    "<br>\n" +
                    "<br>\n" +
                    "<div class=\"comment-content-test\"></div>" +
                    "<div class=\"add-comment-section\"></div>" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "                </div>\n"
                );

                let postId = "post-" + el.id;

                if (el.videoId) {
                    $("#post-conteiner").find("#" + postId + " .post-video-test").append(
                        "<br/>" +
                        "<iframe width=\"600\" height=\"500\" src= 'https://www.youtube.com/embed/"+ el.videoId +"' frameborder=\"0\" \n" +
                        "                allow=\"accelerometer; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen>\n" +
                        "        </iframe>" +
                        "<br/>"
                    );
                }

                if (el.photo) {
                    $("#post-conteiner").find("#" + postId + " .photo-section-add").append(
                        "<p><span><img src='images/" + el.photo + "' class='img-rounded' alt='Cinque Terre' width='450' height='450'></span></p>\n" +
                        "<br/>"
                    );
                }

                // console.log($("#post-conteiner").find("#" + test));
                el.comments.forEach(function (comment) {
                    // console.log("yes maybe");
                    $("#post-conteiner").find("#" + postId + " .comment-content-test").append(
                        " <div id='comment-" + comment.id + "' class=\"col-md-12 commentsblock border-top panel-comment-like total-likes\">\n" +
                        "                    <div class=\"media\">\n" +
                        "                        <div class=\"media-left\"><a href=\"/user"+comment.userLoginUsername+"\" > <img alt=\"64x64\"\n" +
                        "                                                                                   src= 'images/" + comment.userPicture + "'\n" +
                        "                                                                                   class=\"media-object\"> </a></div>\n" +
                        "                        <div class=\"media-body\">\n" +
                        "                            <br>\n" +
                        "                            <h4 class=\"media-heading\"><span>" + comment.userFirstName + " " + comment.userLastName + "</span></h4>\n" +
                        "                            <small><i class=\"fa fa-clock-o\"></i>  " + comment.creationTime + "</small></h4> " +
                        "                            <p><span>" + comment.text + "</span></p>\n" +
                        "                        <span class=\"like-dislike-button-Comment pull-right\" ></span>" +
                        "                        <span class='total-likes-value-comment pull-right'> " + comment.totalLikes + " </span>" +
                        "                        </div>\n" +
                        "                    </div>\n" +
                        "                </div>"
                    );
                    let commentId = "comment-" + comment.id;

                    console.log(comment.id);
                    if (!comment.liked) {
                        $("#post-conteiner").find("#" + commentId + " .like-dislike-button-Comment")
                            .append(
                                "<a class=\"pull-right btn btn-sm btn-primary button-like-comment\"><span class=\"glyphicon glyphicon-thumbs-up\"></span> </a>"
                            )
                    } else {
                        $("#post-conteiner").find("#" + commentId + " .like-dislike-button-Comment")
                            .append(
                                "<a class=\"pull-right btn btn-sm btn-danger button-dislike-comment\"><span class=\"glyphicon glyphicon-thumbs-down\"></span> </a>"
                            )
                    }
                });


                $("#post-conteiner").find("#" + postId + " .add-comment-section").append(
                    "<div class=\"col-md-12 border-top comment-replay\">\n" +
                    "                                <div class=\"status-upload\">\n" +
                    "                                        <label>Comment</label>\n" +
                    "                                                                                                                      \n" +
                    "                                        <textarea class=\"comment-content form-control\" placeholder=\"Comment here\"></textarea>\n" +
                    "                                        <br>\n" +
                    "                                        <ul class=\"nav nav-pills pull-left \">\n" +
                    // "                                            <li><a title=\"\"><i class=\"glyphicon glyphicon-picture\"></i></a></li>\n" +
                    "                                        </ul>\n" +
                    "<button id=\"submit-comment\" type=\"submit\" class=\"btn btn-success pull-right submit-comment testSubmit\"> Comment</button>\n" +
                    "<span class=\"like-dislike-button pull-right\" ></span>                                       " +
                    "                                      \n" +
                    "                                </div>\n" +
                    "                            </div>"
                );

                if (!el.liked) {
                    $("#post-conteiner").find("#" + postId + " .add-comment-section .like-dislike-button")
                        .append(
                            "<div class=\"btn btn-primary button-like btn-primary\"><span class=\"glyphicon glyphicon-thumbs-up\"></span> Like</div>"
                        )
                } else {
                    $("#post-conteiner").find("#" + postId + " .add-comment-section .like-dislike-button")
                        .append(
                            "<div class=\"btn button-dislike btn-primary btn-danger\"><span class=\"glyphicon glyphicon-thumbs-down\"></span> Dislike</div>"
                        )
                }
            });
        },
        error: function (err) {
            console.log(err);
            console.log("error");
        }
    })
}


function getFriends() {
    fetch("http://localhost:8080/friends/" + userName, {
        method: "GET",
        headers: {
            "Content-type": "application/json",
            "Accept": "application/json, text/plain, */*"
        }
    })
        .then((res) => res.json())
        .then(function (fr) {
            visualiseFriends(fr);
        })
        .catch((err) => {
            console.log(err.message);
        })
}

function visualiseFriends(fr) {
    console.log(fr);

    for (let i = 0; i < fr.length; i++) {

        let userName = fr[i].senderLoginUserName;
        let friendSenderFirstName = fr[i].senderFirstName;
        let friendSenderLastName = fr[i].senderLastName;
        let friendSenderPicture = fr[i].senderPicture;

        friend = `
                            <a href="/user${userName}" class="member">
                                <img src= 'images/${friendSenderPicture}' alt="">
                                <div
                                class="memmbername"><span> ${friendSenderFirstName} ${friendSenderLastName} </span>
                                </div>
                            </a>`;

        let $friendContainer = $(".memberblock");

        $friendContainer.append(friend);
    }
}


function addComment() {
    $("#post-conteiner").on("click", ".testSubmit", function () {
        let $totalComments = $(this).closest(".panel-default").find(".total-comments-value");
        let totalCommentsVal = parseInt($totalComments.text());
        let $postId = $(this).closest(".panel-default").attr("id");
        let postIdNumber = $postId.substr(5);
        let $commentContent = $(this).parent().find(".comment-content");
        let commentContentText = $commentContent.val();

        let obj = {
            "postId": postIdNumber,
            "text": commentContentText
        };

        fetch("http://localhost:8080/users/comments", {
            method: "POST",
            body: JSON.stringify(obj),
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            }
        })
            .then((res) => res.json())
            .then(function (response) {

                $("#post-conteiner").find("#" + $postId + " .comment-content-test").append(
                    " <div class=\"col-md-12 commentsblock border-top\">\n" +
                    "                    <div class=\"media\">\n" +
                    "                        <div class=\"media-left\"><a href=\"javascript:void(0)\"> <img alt=\"64x64\"\n" +
                    "                                                                                  src= 'images/" + response.userPicture + "'\n" +
                    "                                                                                   class=\"media-object\"> </a></div>\n" +
                    "                        <div class=\"media-body\">\n" +
                    "                            <br>\n" +
                    "                            <h4 class=\"media-heading\"><span> " + response.userFirstName + " " + response.userLastName + "</span></h4>\n" +
                    "                            <small><i class=\"fa fa-clock-o\"></i>  " + response.creationTime + "</small></h4> " +
                    "                            <p><span> " + response.text + " </span></p>\n" +
                    "                        <span class=\"like-dislike-button-Comment pull-right\" ></span>" +
                    "                        <a class=\"pull-right btn btn-sm btn-primary button-like-comment\"><span class=\"glyphicon glyphicon-thumbs-up\"></span> </a>" +
                    "                        <span class='total-likes-value-comment pull-right'> " + response.totalLikes + " </span>" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>");

                $commentContent.val("");
                $totalComments.text(totalCommentsVal + 1);
            })
            .catch((err) => {
                console.log(err.message);
            })
    })
}

function extractYoutubeId(url){
    let regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    let match = url.match(regExp);
    return (match&&match[7].length==11)? match[7] : false;
}

function addPost() {

    $("#submit-post").on("click", function () {
        let $postContent = $("#post-content").val();
        let $postVisibility = $("#post-visibility").val();
        let $postPhoto = $("#contentT").val().substr(12);

        let obj = undefined;
        let $videoUploadUrl = $('#video-upload-input').val();

        if ($videoUploadUrl === null || $videoUploadUrl === undefined) {
            obj = {
                "text": $postContent,
                "postAccessibility": $postVisibility,
                "photo": $postPhoto
            };
        } else {
            $videoUploadUrl = extractYoutubeId($videoUploadUrl);
            obj = {
                "text": $postContent,
                "postAccessibility": $postVisibility,
                "videoId": $videoUploadUrl,
                "photo": $postPhoto
            };
        }

        fetch("http://localhost:8080/posts/", {
            method: "POST",
                body: JSON.stringify(obj),
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            }
        })
            .then(handleErrors)
            .then((res) => res.json())
            .then(function (el) {
                console.log(el);
                $("#post-conteiner").prepend(
                    "<div id='post-" + el.id + "' class=\"panel panel-default\">\n" +
                    "                <div class=\"col-md-12\">\n" +
                    "                    <div class=\"media\">\n" +
                    "                        <div class=\"media-left\"><a href=\"javascript:void(0)\"> <img\n" +
                    "                                 src= 'images/" + el.userPicture + "' alt=\"\" class=\"media-object\">\n" +
                    "                        </a></div>\n" +
                    "                        <div class=\"media-body\">\n" +
                    "                            <br>\n" +
                    "<button id=\"delete-post\" type=\"submit\" class=\"btn btn-warning pull-right delete-post-wall\"> Delete</button>" +
                    "                            <h4 class=\"media-heading\"><span>" + el.userFirstName + " " + el.userLastName + " </span> <br>\n" +
                    "                                <small><i class=\"fa fa-clock-o\"></i>  " + el.creationTime + "</small></h4>\n" +
                    "                            <p><span>" + el.text + "</span></p>\n" +
                    "<div class='photo-section-add'></div>" +
                    "\n" +
                    "<div class='video-section-add'></div>" +
                    "                            <ul class=\"nav nav-pills pull-left \">\n" +
                    "                                <li><a href=\"\" title=\"\"><i class=\"glyphicon glyphicon-thumbs-up\"></i>" +
                    "<span> " + el.totalLikes + " </span> </a></li>\n" +
                    "                                <li><a href=\"\" title=\"\"><i class=\" glyphicon glyphicon-comment\"></i> 0</a></li>\n" +
                    "                            </ul>\n" +
                    "<br>\n" +
                    "<br>\n" +
                    "<div class=\"comment-content-test\"></div>" +
                    "<div class=\"add-comment-section\"></div>" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "                </div>\n"
                );

                let postId = "post-" + el.id;

                if (el.videoId) {
                    $("#post-conteiner").find("#" + postId + " .video-section-add").append(
                        "<br/>" +
                        "<iframe width=\"600\" height=\"500\" src= 'https://www.youtube.com/embed/"+ el.videoId +"' frameborder=\"0\" \n" +
                        "                allow=\"accelerometer; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen>\n" +
                        "        </iframe>" +
                        "<br/>"
                    );
                }

                if (el.photo) {
                    $("#post-conteiner").find("#" + postId + " .photo-section-add").append(
                        "<p><span><img src='images/" + el.photo + "' class='img-rounded' alt='Cinque Terre' width='450' height='450'></span></p>\n" +
                        "<br/>"
                    );
                }

                $("#post-conteiner").find("#post-" + el.id + " .add-comment-section").append(
                    "<div class=\"col-md-12 border-top comment-replay\">\n" +
                    "                                <div class=\"status-upload\">\n" +
                    "                                        <label>Comment</label>\n" +
                    "                                                                                                                      \n" +
                    "                                        <textarea class=\"comment-content form-control\" placeholder=\"Comment here\"></textarea>\n" +
                    "                                        <br>\n" +
                    "                                        <button id=\"submit-comment\" type=\"submit\" class=\"btn btn-success pull-right submit-comment testSubmit\"> Comment</button>\n" +
                    "                                        <div class=\"pull-right btn btn-primary button-like btn-primary\"><span class=\"glyphicon glyphicon-thumbs-up\"></span> Like</div>" +
                    "                                      \n" +
                    "                                </div>\n" +
                    "                            </div>"
                );

                $("#contentT").val("");
                $("#post-content").val("");
            })
            .catch((err) => {
                $("#creation-post-error").append("Something Went Wrong. Try Again").css("color", "red");
                $("#creation-post-error").append(
                    "<button type=\"button\" class=\"close post-close-message\" aria-label=\"Close\">\n" +
                    "  <span aria-hidden=\"true\">&times;</span>\n" +
                    "</button>"
                );
                console.log(err.message);
            })
    })
}

function likePost() {
    $("#post-conteiner").on("click", ".like-dislike-button", function () {
        let $totalLikes = $(this).closest(".panel-default").find(".total-likes-value");
        let $likeContent = $(this);
        let postIdNumber = $(this).closest(".panel-default").attr("id").substr(5);
        let obj = {
            "postId": postIdNumber
        };

        if ($(this).find(".button-like").hasClass("button-like")) {
            fetch("http://localhost:8080/posts/like", {
                method: "POST",
                body: JSON.stringify(obj),
                headers: {
                    "Content-type": "application/json",
                    "Accept": "application/json, text/plain, */*"
                }
            })
                .then((res) => res.json())
                .then(function (el) {
                    $totalLikes.text(el);
                    $($likeContent).empty();
                    $($likeContent).append(
                        "<div class=\"btn button-dislike btn-primary btn-danger\"><span class=\"glyphicon glyphicon-thumbs-down\"></span> Dislike</div>"
                    );
                })
                .catch((err) => {
                    console.log(err.message);
                })
        } else {
            fetch("http://localhost:8080/posts/dislike", {
                method:"DELETE",
                body: JSON.stringify(obj),
                headers: {
                    "Content-type": "application/json",
                    "Accept": "application/json, text/plain, */*"
                }
            })
                .then((res) => res.json())
                .then(function (el) {
                    // console.log(el);
                    // console.log($totalLikes);
                    $totalLikes.text(el);
                    $($likeContent).empty();
                    $($likeContent).append(
                        "<div class=\"btn btn-primary button-like btn-primary\"><span class=\"glyphicon glyphicon-thumbs-up\"></span> Like</div>"
                    );
                })
                .catch((err) => {
                    console.log(err.message);
                });
        }
    })
}


function fillEditPopup() {

    let testVar = undefined;
    let $newOne = undefined;

    $("#table-content").on("click", ".edit-user-final", function () {
        let userId = $(this).closest("#table-content").find(".user-id-current").attr("id");
        $newOne = $(this).closest(".some-content-here").find(".users-list-name");
        testVar = userId;
        fetch("http://localhost:8080/users/logged", {
            method: "GET",
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            }
        })
            .then((res) => res.json())
            .then(function (ele) {
                $("#first-name-edit").val(ele.firstName);
                $("#last-name-edit").val(ele.lastName);
                $("#email-edit").val(ele.email);
                $("#description-edit").val(ele.description);
                $("#age-edit").val(ele.age);
                $("#gender-edit").val(ele.gender);
                $("#country-edit").val(ele.country);
            })
            .catch((err) => {
                console.log(err.message);
            })
    });

    $(".submit-edit").on("click", function () {
        let obj = {
            "id": testVar,
            "firstName": $("#first-name-edit").val(),
            "lastName": $("#last-name-edit").val(),
            "email": $("#email-edit").val(),
            "description": $("#description-edit").val(),
            "age": $("#age-edit").val(),
            "gender": $("#gender-edit").val(),
            "country": $("#country-edit").val()
        };

        fetch("http://localhost:8080/users/", {
            method: "PUT",
            body: JSON.stringify(obj),
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            }
        })
            .then((res) => res.json())
            .then(function (el) {
                console.log(el);
                $($newOne).text(el.firstName);
            })
            .catch((err) => {
                console.log(err.message);
            })
    })
}


function likeComment() {
    $("#post-conteiner").on("click", ".like-dislike-button-Comment", function () {
        let $totalLikes = $(this).closest(".total-likes").find(".total-likes-value-comment");
        let $likeContent = $(this);
        let commentIdNumber = $(this).closest(".commentsblock").attr("id").substr(8);

        let obj = {
            "commentId": commentIdNumber
        };

        if ($(this).find(".button-like-comment").hasClass("button-like-comment")) {
            fetch("http://localhost:8080/comments/like", {
                method: "POST",
                body: JSON.stringify(obj),
                headers: {
                    "Content-type": "application/json",
                    "Accept": "application/json, text/plain, */*"
                }
            })
                .then((res) => res.json())
                .then(function (el) {
                    $totalLikes.text(el);
                    $($likeContent).empty();
                    $($likeContent).append(
                        "<a class=\"pull-right btn btn-sm btn-danger button-dislike-comment\"><span class=\"glyphicon glyphicon-thumbs-down\"></span> </a>"
                    );
                })
                .catch((err) => {
                    console.log(err.message);
                })
        } else {
            fetch("http://localhost:8080/comments/dislike", {
                method: "DELETE",
                body: JSON.stringify(obj),
                headers: {
                    "Content-type": "application/json",
                    "Accept": "application/json, text/plain, */*"
                }
            })
                .then((res) => res.json())
                .then(function (el) {
                    // console.log(el);
                    // console.log($totalLikes);
                    $totalLikes.text(el);
                    $($likeContent).empty();
                    $($likeContent).append(
                        "<a class=\"pull-right btn btn-sm btn-primary button-like-comment\"><span class=\"glyphicon glyphicon-thumbs-up\"></span> </a>"
                    );
                })
                .catch((err) => {
                    console.log(err.message);
                });
        }
    })
}

function handleErrors(response) {
    if (!response.ok) throw Error(response.statusText);
    return response;
}

function hidePostErrorMessage() {
    $("#creation-post-error").on("click", function () {
        $("#creation-post-error").empty();
    })
}

function deletePostWall() {
    $("#post-conteiner").on("click", ".delete-post-wall", function () {
        let $postBlock = $(this).closest(".panel-default");
        let postId = $postBlock.attr("id");
        let delimiter = postId.indexOf("-");
        postId = postId.substr(parseInt(delimiter) + 1);
        console.log($postBlock);
        console.log($postBlock.attr("id"));
        console.log(postId);

        $.ajax({
            url: window.location.origin + "/posts/" + postId,
            type: "DELETE",
            success: function () {
                $postBlock.empty();
            },
            error: function (err) {
                console.log(err);
                console.log("error");
            }
        })
    })
}