"use strict";
$(function () {
    searchUserByFirstName();
    getAndLoadContent(9, 0);
    nextPage();
    previousPage();
    getAndLoadContentRequests(9, 0);
});


function searchUserByFirstName() {
    $("#search-user-field").keyup(function () {
        let searchBoxValue = this.value;
        if (searchBoxValue.length === 0) {
            getAndLoadContent(9, 0);
        } else {
            let url = new URL("http://localhost:8080/users/filter");
            let params = {firstName: searchBoxValue, lastName: searchBoxValue};
            url.search = new URLSearchParams(params).toString();

            fetch(url, {
                method: "GET",
                headers: {
                    "Content-type":"application/json",
                    "Accept":"application/json, text/plain, */*"
                }
            })
                .then((res) => res.json())
                .then(function (el) {
                    if (el.length === 0) {
                        $("#table-content").empty();
                    } else {
                        let $tableContent = $("#table-content");
                        $tableContent.empty();

                        el.forEach(function (el) {
                            $tableContent.append(
                                "<div id='user-" + el.id + "' class='col-4 some-content-here'>" +
                                "<figure class='rectangle-form'>" +
                                "  <a href='/user" + el.loginUsername + "'> <img src= 'images/" + el.picture + "' alt=\"\"> </a>" +
                                "<figcaption cla>" +
                                "   <div  style='font-size: smaller' class='users-list-name'>"+ el.firstName + " " + el.lastName +"</div>" +
                                "   <div class='row button-section'>" +
                                "       <a href=\"#\" class=\"btn btn-primary add-friend-button btn-primary\"><span class=\"glyphicon glyphicon-send\"></span> Add</a>" +
                                "       <div class='col-6'>" +
                                "           <a   href= '/user/" + el.loginUsername + "' class=\"btn btn-primary btn-success\"><span class=\"glyphicon glyphicon-eye-open\"></span> View</a>" +
                                "       </div>" +
                                "   </div>" +
                                "</figcaption>" +
                                "</figure>" +
                                "</div>"
                            );
                        });
                    }
                })
                .catch((err) => {
                    console.log(err.message);
                });
        }
    })
}

function previousPage() {
    $("#previous-page").on("click", function () {
        getAndLoadContent(9, parseInt($("#current-page-value").text()) - 2);
    })
}

function nextPage() {
    $("#next-page").on("click", function () {
        getAndLoadContent(9, parseInt($("#current-page-value").text()));
    })
}

function getAndLoadContent(size, page) {
    $.ajax({
        url: window.location.origin + "/users/page",
        type: "GET",
        data: {
            size: size,
            page: page,
        },
        success: function (response) {
            fetch("http://localhost:8080/friends/check", {
                method: "PUT",
                body: JSON.stringify(response.content),
                headers: {
                    "Content-type": "application/json",
                    "Accept": "application/json, text/plain, */*"
                }
            })
                .then((res) => res.json())
                .then(function (ele) {
                    let $tableContent = $("#table-content");
                    $tableContent.empty();
                    ele.forEach(function (el) {
                        $tableContent.append(
                            "<div id='user-" + el.id + "' class='col-4 some-content-here'>" +
                            "<figure class='rectangle-form'>" +
                            "  <a href='/user/" + el.loginUsername + "'> <img src= 'images/" + el.picture + "' alt=\"\"> </a>" +
                            "<figcaption cla>" +
                            "   <div  style='font-size: smaller' class='users-list-name'>"+ el.firstName + " " + el.lastName +"</div>" +
                            "   <div class='row button-section'>" +
                            "       <a href=\"#\" class=\"btn btn-primary add-friend-button btn-primary\"><span class=\"glyphicon glyphicon-send\"></span> Add</a>" +
                            "       <div class='col-6'>" +
                            "           <a   href= '/user/" + el.loginUsername + "' class=\"btn btn-primary btn-success\"><span class=\"glyphicon glyphicon-eye-open\"></span> View</a>" +
                            "       </div>" +
                            "   </div>" +
                            "</figcaption>" +
                            "</figure>" +
                            "</div>"
                        );

                        if (el.hasFriend) {
                            $("#table-content").find("#user-" + el.id + " .add-friend-button").hide();
                        }
                    });
                });

            $("#current-page-value").text(response.pageable.pageNumber + 1);
            enableDisableButton($("#previous-page"), response.first);
            enableDisableButton($("#next-page"), response.last);
        },
        error: function (err) {
            console.log(err);
            console.log("error");
        }
    })
}

function enableDisableButton($button, shouldDisabled) {
    if (shouldDisabled) {
        $button.addClass("li-disabled");
    } else if ($button.hasClass("li-disabled")) {
        $button.removeClass("li-disabled");
    }
}

function getAndLoadContentRequests(size, page) {
    $.ajax({
        url: window.location.origin + "/request/page",
        type: "GET",
        data: {
            size: size,
            page: page,
        },
        success: function (response) {
            console.log(response);
            let $requestContent = $("#request-content");
            $requestContent.empty();
            response.content.forEach(function (el) {
                $requestContent.append(
                    "<div id='request-" + el.receiverId + "-" + el.senderId +"' class='col-4 some-content-here'>" +
                    "<figure class='rectangle-form'>" +
                    "   <a href='/user/" + el.senderLoginUsername + "'>  <img src= 'images/" + el.senderPicture + "' alt=\"\"> </a>" +
                    "<figcaption cla>" +
                    "   <div  style='font-size: smaller' class='users-list-name'>"+ el.senderFirstName + " " + el.senderLastName +"</div>" +
                    "   <div class='row'>" +
                    "      <a href=\"#\" class=\"btn btn-sm accept-request btn-primary\"><span class=\"thumthum\"></span> Accept</a>" +
                    "       <div class='col-6'>" +
                    "          <a href=\"#\" class=\"btn btn-sm decline-request btn-danger\"><span class=\"glyphicon glyphicon-trash\"></span> Del</a>" +
                    "       </div>" +
                    "   </div>" +
                    "</figcaption>" +
                    "</figure>" +
                    "</div>"
                )
            });
            $("#current-page-value").text(response.pageable.pageNumber + 1);
            enableDisableButton($("#previous-page"), response.first);
            enableDisableButton($("#next-page"), response.last);
        },
        error: function (err) {
            console.log(err);
            console.log("error");
        }
    })
}